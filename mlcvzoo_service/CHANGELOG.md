# MLCVZoo mlcvzoo_service module Versions:

4.0.0 (2025-01-28):
------------------
Integrate new models into the mlcvzoo-service

- The models are from the following application areas:
* Rotated Object Detection
* Text Recognition
* Classification
* Segmentation

- Adapt from mlcvzoo_service_runtime to mlcvzoo_service_jobs as runtime for jobs

3.1.1 (2024-09-27):
------------------
Improve the local docker compose setup:

- Use the correct base image for the container runtime image
- Update README to be more precise on the local setup
- Fix MLflow bug in mlcvzoo_service_runtime

3.1.0 (2024-09-12):
------------------
Add functionality to view job logs:

- Extend JobManager and DockerService to provide logs of a specific job
- Add endpoint to get logs of a specific job

3.0.0 (2024-07-16):
------------------
Implement a multi container architecture:

- Add a service to start docker containers during runtime
- Move the execution of training and prediction jobs to dedicated runtime containers
- Add a submodule mlcvzoo_service_runtime to provide all necessary logic for runtime containers
  i.e. loading data, executing training/ prediction and logging results

2.1.0 (2024-06-28):
------------------
Adapt to new geometric evaluation introduced by mlcvzoo-base 6.0.0

- Use feature of logging false-positive and false-negative images to mlflow
- Tidy up project dependencies:
  - Only use MLCVZoo modules that are currently relevant
  - Drop: mlcvzoo-tf-classification, mlcvzoo-darknet, mlcvzoo-mmocr

2.0.6 (2024-06-24):
------------------
Upgrade MLflow version to 2.11.3

2.0.5 (2024-06-21):
------------------
Adapt python package management:

- Replace poetry by uv as dependency resolver and installer
- Replace poetry by the python native build package for building the python package

2.0.4 (2024-04-09):
------------------
Fix evaluation being run twice

2.0.3 (2024-02-20)
------------------
Minor fixes:

- Fix data decoding for training and prediction jobs
- Use new feature of the config-builder: Tuple save decoding of dictionaries
- Update the mlcvzoo-mmdetection package to version >=6.4, to use the feature
  'Automatically set class-mapping config for nested datasets' for training RTMDet models
- Remove custom pypi index from runtime Dockerfile

2.0.2 (2024-02-07)
------------------
Updated links in pyproject.toml

2.0.1 (2024-01-23)
------------------
Fix docker runtime

- Change uwsgi config to use correct module name after flask-restx integration

2.0.0 (2024-01-04)
------------------
Implement flask-restx

- Update the module and test structure of the endpoints to be more granular
- Improve documentation of endpoints and provide Swagger documentation
- Standardize the return values of individual endpoints

1.2.0 (2024-01-04)
------------------
Add job overview

- Endpoint to get all jobs and their current status

1.1.0 (2024-01-04)
------------------
Add TrainingJob status

- Logging TrainingJob status in mlflow
- Refactor code to use mlflow client instead of active runs for logging

1.0.0 (2023-11-09)
------------------
Add prediction Component

- Add a prediction component to be able to execute prediction jobs i.e. to predict objects in one
  or more images
- Refactor the code and create a base job class which serves as a parent for current and future
  jobs

0.2.2 (2023-08-28)
------------------

- Compatibility to mlcvzoo-mmdetection version 6: Correct decoding of tuples in json string

0.2.1 (2023-07-05)
------------------

- Fix a bug that caused objects not to be loaded from a directory in an S3 storage

0.2.0 (2023-06-15)
------------------

- Use the pagination feature of the boto3 S3Client to load more than 1000 objects of a given s3
  location
- Add a TensorboardLoggingConfig to the ModelEvaluatorCLIConfig in order to use the functionality
  of 'log_false_positive_info_to_tb' in the log_results method of the ODMetricFactory
- Relicense to OLFL-1.3 which succeeds the previous license

0.1.0 (2023-03-01)
------------------

- Initial release of the package
