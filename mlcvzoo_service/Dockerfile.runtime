FROM nvidia/cuda:11.7.1-cudnn8-devel-ubuntu22.04

# For NVIDIA Jetson devices, select one or more appropriate CUDA arch values below
# Look up your CUDA compute capability at: https://developer.nvidia.com/cuda-gpus
# Some common CUDA compute Capabilites for x86:
#  - Quadro P5000/Geforce RTX10*0: 6.1
#  - Tesla V100: 7.0
#  - Quadro T1000/Geforce RTX20*0: 7.5
#  - NVIDIA A100: 8.0
#  - RTX A*000/Geforce RTX3090: 8.6
# Some common CUDA compute capabilities for Jetson devices (arm64):
#  - Jetson Nano/TX1/Tegra X1: 5.3
#  - Jetson TX2: 6.2
#  - Jetson Xavier AGX: 7.2
ENV TORCH_CUDA_ARCH_LIST "6.1;7.0;7.5;8.0;8.6"
ENV CUDA_HOME="/usr/local/cuda/"
ENV NVIDIA_DISABLE_REQUIRE=1

# Fetch updated NVIDIA keys
RUN apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/3bf863cc.pub && \
    apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/7fa2af80.pub


# INFORMATION about packages:
# - gettext-base is needed for envsubst which is used in CI
# - libgl1-mesa-glx is needed by opencv-python, otherwise it
#   results in an 'ImportError: libGL.so.1'
# - libprotobuf-dev and protobuf-compiler are needed by onnx
# - libpq-dev is needed by postgresql
# - blas, lapack, fftw3 and hdf5 are needed by pytorch et. al.
# - Keep default GCC (see compatibility matrix here: https://docs.nvidia.com/deeplearning/cudnn/pdf/cuDNN-Support-Matrix.pdf)
#   * GCC-7 for Jetson builds (automatically via ubuntu 18.04)
#   * GCC-9 for AMD64 and CUDA 11 (automatically via ubuntu 20.04)
RUN echo ">------ Update system, install packages" && \
    apt update && \
    DEBIAN_FRONTEND="noninteractive" apt install --no-install-recommends -y \
    nginx uwsgi uwsgi-plugin-python3 \
    build-essential gettext-base cmake ninja-build libprotobuf-dev protobuf-compiler \
    python3-pip python3-setuptools python3-venv libpython3-dev  \
    git tree curl unzip nano \
	  pkg-config libgl1-mesa-glx libcap-dev libpq-dev libopencv-dev \
    libblas3 liblapack3 liblapack-dev libblas-dev gfortran libatlas-base-dev \
    libfftw3-bin libfftw3-dev libhdf5-dev libhdf5-mpi-dev libhdf5-openmpi-dev \
    && apt clean && rm -rf /var/lib/apt/lists/*

RUN curl -LsSf https://astral.sh/uv/install.sh | sh

ENV PIP_NO_CACHE_DIR=1
ENV PATH "/root/.local/bin:$PATH:${HOME}/.cargo/bin"

ENV PROJECT_DIR="/srv/mlcvzoo-service/"
COPY . $PROJECT_DIR
WORKDIR $PROJECT_DIR

# Disable CUDA detection to build with CUDA without a GPU
ENV FORCE_CUDA="1"

# Define arguments for being able to use the ml-toolbox token for the installation
# of git+https branch dependencies. Comment in when needed
#ARG ML_TOOLBOX_TOKEN_NAME
#ARG ML_TOOLBOX_TOKEN_PW

ARG PYPI_MIRROR="https://nexus.apps.sele.iml.fraunhofer.de/repository/pypi-group/simple"
RUN pip config set global.index-url "$PYPI_MIRROR"

ENV BUILD_ENV_DIR="/build-env/"
ENV VIRTUAL_ENV="${BUILD_ENV_DIR}venv/"
ENV REQUIREMENTS_FILE="${PROJECT_DIR}requirements_locked/requirements-lock-uv-py310-all.txt"
ENV PATH="${VIRTUAL_ENV}bin:$PATH"
RUN python3 -m venv $VIRTUAL_ENV \
  && python3 -m pip install --upgrade pip \
  && ./build.sh

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/uwsgi.ini /etc/uwsgi/uwsgi.ini
RUN chmod ugo+rx /etc/uwsgi/ /etc/nginx/; chmod ugo+r /etc/nginx/nginx.conf /etc/uwsgi/uwsgi.ini
RUN mkdir -p /var/log/nginx/ && chmod --recursive ugo+rwx /var/log/nginx/
RUN mkdir -p /var/lib/nginx/body && chmod --recursive ugo+rwx /var/lib/nginx/
RUN chmod ugo+rwx /run

RUN chmod --recursive ugo+rw $PROJECT_DIR

# Cleanup uv cache
RUN uv cache clean

ENV TENSORBOARD_DIR="/tb_logs"
RUN mkdir /tb_logs

EXPOSE 11080/tcp
# socket permissions are not read from uwsgi config file so set them here
CMD nginx & exec uwsgi --ini /etc/uwsgi/uwsgi.ini --chmod-socket=666

LABEL org.opencontainers.image.authors="Maximilian Otten <maximilian.otten@iml.fraunhofer.de>, Luca Kotulla <luca.kotulla@iml.fraunhofer.de" \
      org.opencontainers.image.vendor="Fraunhofer IML" \
      org.opencontainers.image.title="MLCVZoo Service Runtime Image" \
      org.opencontainers.image.description="Main GPU-enabled runtime container for manging prediction and training jobs"
