# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Module defining a manager for incoming jobs.
"""

import logging
import os
from collections import deque
from threading import Lock
from typing import Any, Dict, Optional, Tuple, Union

from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter

from mlcvzoo_service.services.container.constants import ImplementedContainerServices
from mlcvzoo_service.services.jobs.constants import JobStatus
from mlcvzoo_service.services.jobs.prediction.prediction_job import PredictionJob
from mlcvzoo_service.services.jobs.training.training_job import TrainingJob

logger = logging.getLogger(__name__)


class _JobManager:
    """Manage incoming prediction and training jobs."""

    def __init__(self) -> None:
        self.lock = Lock()
        self._gpu_map: Dict[int, Optional[Union[PredictionJob, TrainingJob]]]
        self._endpoint: str
        self._queued_jobs: deque[PredictionJob | TrainingJob]
        self._finished_jobs: deque[PredictionJob | TrainingJob]
        self._crashed_jobs: deque[PredictionJob | TrainingJob]
        self._running_jobs: Dict[
            str, PredictionJob | TrainingJob
        ] = {}  # order of jobs does not matter, therefore can be a dict

        self.__set_gpu_map()
        self.__init_job_lists()
        self.__init_container_service()
        self.__load_service_endpoint()

    def __init_job_lists(self) -> None:
        """Initialize all necessary job lists.

        The job lists use deques as the underlying datastructure because they are a generalization
        of Queues and therefore designed to store/ exchange data between threads. In contrast to
        Queues, they are designed to be used as a data structure and therefore allow some
        operations convenient for the JobManager.

        The necessary job lists are:
            - queued_jobs: All added jobs which are not started yet. First-in first-out principal
                            so the order of the jobs matters.
            - finished_jobs: All successfully finished jobs.
            - crashed_jobs: All crashed jobs.
        """
        self._queued_jobs = deque()
        self._finished_jobs = deque()
        self._crashed_jobs = deque()

    def __load_service_endpoint(self) -> None:
        """Load the service endpoint.

        Raises:
            RuntimeError if the SERVICE_ENDPOINT is not set
        """
        endpoint = os.getenv("SERVICE_ENDPOINT")
        if endpoint is None:
            raise RuntimeError(
                "SERVICE_ENDPOINT is not set! Set an environment variable "
                "'SERVICE_ENDPOINT' with the mlcvzoo-service endpoint as value."
            )
        self._endpoint = endpoint

    def __set_gpu_map(self) -> None:
        """Get the GPU map.

        Raises:
            RuntimeError: If GPUS is not set.
        """
        gpus = os.getenv("GPUS")
        if gpus is None:
            raise RuntimeError(
                "GPUS environment variable is not set! Set an environment variable 'GPUS' and "
                "specify all available gpus as a list of comma separated IDs, e.g.:\n"
                "GPUS=0,1,2"
            )
        with self.lock:
            self._gpu_map = {int(gpu): None for gpu in gpus.split(",")}

    def __init_container_service(self) -> None:
        """Initialize the container service.

        Raises:
            ValueError: The specified container service is not implemented.
        """
        container = os.getenv("CONTAINER_SERVICE", "DOCKER")
        if container == ImplementedContainerServices.DOCKER.value:
            from mlcvzoo_service.services.container.docker.service import (
                global_docker_service,
            )

            self._container_service = global_docker_service

        else:
            raise ValueError(
                f"Specified CONTAINER_SERVICE={container} is not supported. Supported container "
                f"services are: {[service for service in ImplementedContainerServices]}"
            )

    @property
    def all_jobs(self) -> list[PredictionJob | TrainingJob]:
        """Get all jobs.

        As running_jobs is not a deque,

        Returns:
            List with all jobs.
        """
        with self.lock:
            running_jobs = self._running_jobs.values()
        return (
            list(self._queued_jobs)
            + list(running_jobs)
            + list(self._finished_jobs)
            + list(self._crashed_jobs)
        )

    @property
    def queued_jobs(self) -> deque[PredictionJob | TrainingJob]:
        """Get all current jobs in queue.

        Returns:
            List with all jobs in the queue.
        """
        return self._queued_jobs

    @property
    def running_jobs(self) -> list[PredictionJob | TrainingJob]:
        """Get all running jobs.

        Returns:
            List with all running jobs.
        """
        with self.lock:
            return list(self._running_jobs.values())

    @property
    def finished_jobs(self) -> deque[PredictionJob | TrainingJob]:
        """Get all finished jobs.

        Returns:
            List with all finished jobs.
        """
        return self._finished_jobs

    @property
    def crashed_jobs(self) -> deque[PredictionJob | TrainingJob]:
        """Get all crashed jobs.

        Returns:
            List with all finished jobs.
        """
        return self._crashed_jobs

    @property
    def gpu_map(self) -> Dict[int, PredictionJob | TrainingJob | None]:
        """Get the GPU map.

        Returns:
            The GPU map.
        """
        with self.lock:
            return self._gpu_map

    def add_job(self, job: Union[PredictionJob, TrainingJob]) -> str:
        """Add a new job to the manager.

        Args:
            job: Job to add.

        Returns:
            The UUID of the job if it was added successfully, else None.

        Raises:
            RuntimeError: If an error occurs while adding the job.
        """
        try:
            self._queued_jobs.append(job)
            logger.info(f"Added job with id={job.id} to queue")

            if (gpu := self.__check_gpu_availability()) is not None:
                self._start_job(gpu=gpu)  # always start the first job in the queue
            return job.id

        except Exception as e:
            raise RuntimeError(f"Error while adding job: {e}")

    def __check_gpu_availability(self) -> Optional[int]:
        """Check the availability of GPUs.

        Returns:
            The first available GPU.
        """
        with self.lock:
            for gpu, job in self._gpu_map.items():
                if job is None:
                    return gpu
            return None

    def __transition_job_to_running(
        self, gpu: int
    ) -> Union[PredictionJob, TrainingJob]:
        """Transition the first job in the queue to a RUNNING status.

        Transitioning means:
            - switch job from queued to running
            - change job status to RUNNING
            - add job to gpu_map
            - add gpu to job

        Args:
            gpu: GPU to start the job on.
        """
        job = self._queued_jobs.pop()

        with self.lock:
            self._running_jobs[job.id] = job
            job.status = JobStatus.RUNNING
            self._gpu_map[gpu] = job
            job.gpu = gpu

        return job

    def __transition_job_to_finished(
        self,
        job: Union[PredictionJob, TrainingJob],
        results: dict[Any, Any],
    ) -> None:
        """Transition a job to a FINISHED/ CRASHED status.

        Transitioning means:
            - switch job from running jobs to finished or crashed jobs
            - remove job from gpu_map
            - remove gpu from job
            - change job status to FINISHED/ CRASHED
            - add results to job

        Args:
            job: Job to transition.
            results: Results of the job.
        """
        if job.gpu is None:
            raise RuntimeError(
                f"Job with id={job.id} is not running on a GPU. Cannot transition job to "
                f"FINISHED/ CRASHED. This indicates an inconsistent state of the job manager."
            )

        with self.lock:
            del self._running_jobs[job.id]
            self._gpu_map[job.gpu] = None
            job.gpu = None

        self.__store_container_logs(job=job)

        if "crashed" in results:  # if the key exists, the job crashed
            self._crashed_jobs.append(job)
            with self.lock:
                job.status = JobStatus.CRASHED
                job.results = {"message": results["message"]}

        else:
            self._finished_jobs.append(job)
            with self.lock:
                job.status = JobStatus.FINISHED
                job.results = results

    def _start_job(self, gpu: int) -> None:
        """Start a job on a given GPU.

        Args:
            gpu: GPU to start the job on.
        """
        job = self.__transition_job_to_running(gpu=gpu)
        job.container_id = self._container_service.start_container(
            job=job, service_endpoint=f"{self._endpoint}/jobs/{job.id}/finish"
        )
        logger.info(
            f"Started job with id={job.id} on gpu={gpu}. Associated container to the job "
            f"has id={job.container_id}"
        )

    def __store_container_logs(self, job: PredictionJob | TrainingJob) -> None:
        """Store the logs of a container.

        Args:
            job: The job to store the logs for.
        """
        if job.container_id is None:
            logger.error(
                f"Container ID of job with ID '{job.id}' is None. Cannot store logs."
            )
            return

        job_logs = self._container_service.get_container_logs(
            container_id=job.container_id
        )
        job_log_path = self.__get_job_log_path(job_id=job.id)
        job_log_dir = os.path.dirname(job_log_path)

        if not os.path.exists(job_log_dir):
            os.makedirs(job_log_dir)

        with open(job_log_path, "x") as f:
            f.write(job_logs)

        if (
            isinstance(job, TrainingJob)
            and job.mlflow_credentials is not None
            and job.mlflow_credentials.run_id is not None
        ):
            mlflow_adapter = MlflowAdapter(
                credentials=job.mlflow_credentials, set_credentials=True
            )
            mlflow_adapter.log_artifact(
                run_id=job.mlflow_credentials.run_id, local_path=job_log_path
            )

    def finish_job(self, job_id: str, results: dict[Any, Any]) -> None:
        """Finish a given job.

        Args:
            job_id: The ID of the job to finish.
            results: Job results from the associated container.

        Raises:
            RuntimeError: If the job with the given ID was not found in the running jobs.
        """
        if job_id not in self._running_jobs:
            raise RuntimeError(
                f"Job with id={job_id} was not found in the running jobs"
            )

        with self.lock:
            job = self._running_jobs[job_id]

        self.__transition_job_to_finished(job=job, results=results)
        logger.info(f"Finished job with id={job_id}")

        # start next job if queue is not empty and gpu is available
        if len(self._queued_jobs) > 0:
            gpu = self.__check_gpu_availability()
            if gpu is not None:
                self._start_job(gpu=gpu)

    def get_job_status(
        self, job_id: str
    ) -> Union[
        Tuple[JobStatus, None],
        Tuple[JobStatus, Dict[str, Any]],
        Tuple[None, None],
    ]:
        """Get the status of a single job.

        Additionally, the job results are returned

        Args:
            job_id: ID of the job.

        Returns:
            Status and result of the job if it's in the queue 'None' if not.
        """
        for job in self.all_jobs:
            if job.id == job_id:
                return job.status, job.results
        return None, None

    def get_job(self, job_id: str) -> Union[PredictionJob, TrainingJob, None]:
        """Get a job by its ID.

        Args:
            job_id: The ID of the job to get.

        Returns:
            The job with the given ID.
        """
        for job in self.all_jobs:
            if str(job.id) == job_id:
                return job
        return None

    def read_job_logs(self, job_id: str) -> str:
        """Read the logs of a job from the associated log file.

        Returns:
            The logs of the job.
        """
        try:
            with open(self.__get_job_log_path(job_id), "r") as file:
                return file.read()
        except FileNotFoundError as e:
            logger.info(f"Log file for job with id={job_id} not found: {e}")
            return ""

    @staticmethod
    def __get_job_log_path(job_id: str) -> str:
        """Get the absolute path of the log file associated to the given job.

        The log file is stored in the job_log_dir directory under
        /mlcvzoo_service/job_log_dir/<job_id>.txt.

        Args:
            job_id: The ID of the job

        Returns:
            The working directory.
        """
        job_log_dir = os.environ.get(
            "JOB_LOG_DIR", os.path.join(os.getcwd(), "job_logs")
        )
        return f"{job_log_dir}/{job_id}.txt"


# initialize the global job manager here to make it usable by all other modules
global_job_manager = _JobManager()
