# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Abstract base class of a job.
"""
import logging
import uuid
from abc import ABC, abstractmethod
from typing import Any, Dict, List

from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service.services.jobs.constants import JobStatus

logger = logging.getLogger(__name__)


class BaseJob(ABC):
    """
    Encapsulating a single base job in the mlcvzoo service.

    The Jobs in the mlcvzoo-service are used to store the relevant data that is needed to start a
    dedicated job via one of the implemented container services. For now, this is done via
    Docker containers that run a Job via the mlcvzoo-service-runtime module.
    """

    def __init__(
        self,
        image_locations: List[S3DataLocation],
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        model_checkpoint_location: S3DataLocation,
    ):
        self.__id = str(uuid.uuid4())

        self._results: Any = None
        self._status: JobStatus = JobStatus.NEW
        self._gpu: int | None = None
        self._container_id: str | None = None
        logger.info(f"New job with ID='%s' initiated", self.__id)

        self.image_locations: List[S3DataLocation] = image_locations
        self.model_checkpoint_location = model_checkpoint_location
        self.model_type_name = model_type_name
        self.model_configuration_dict = model_configuration_dict

        self.string_replacement_map: Dict[str, str] = {}

    @property
    def container_id(self) -> str | None:
        return self._container_id

    @container_id.setter
    def container_id(self, value: str | None) -> None:
        self._container_id = value

    @property
    def gpu(self) -> int | None:
        return self._gpu

    @gpu.setter
    def gpu(self, value: int | None) -> None:
        self._gpu = value

    @property
    def status(self) -> JobStatus:
        return self._status

    @status.setter
    def status(self, value: JobStatus) -> None:
        self._status = value

    @property
    def id(self) -> str:
        return self.__id

    @property
    def results(self) -> Any:
        return self._results

    @results.setter
    def results(self, value: Any) -> None:
        self._results = value

    @abstractmethod
    def get_environment_variables(self) -> Dict[str, str]:
        """Get the environment variables that are necessary to start a job.

        Returns:
            A dictionary containing the environment variables.
        """
        raise NotImplementedError("This method must be implemented by the subclass")
