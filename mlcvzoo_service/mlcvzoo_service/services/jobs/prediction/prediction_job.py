# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Module defining a prediction job handling the inference of a given model.
"""

from __future__ import annotations

import json
import logging
from typing import Any, Dict, List

from config_builder.json_encoding import TupleEncoder
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service.services.jobs.base.base_job import BaseJob

logger = logging.getLogger(__name__)


class PredictionJob(BaseJob):
    """
    Encapsulates a single prediction job in the mlcvzoo service.
    """

    def __init__(
        self,
        image_locations: List[S3DataLocation],
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        model_checkpoint_location: S3DataLocation,
        second_model_type_name: str | None = None,
        second_model_configuration_dict: Dict[str, Any] | None = None,
        second_model_checkpoint_location: S3DataLocation | None = None,
    ):
        BaseJob.__init__(
            self,
            image_locations=image_locations,
            model_type_name=model_type_name,
            model_configuration_dict=model_configuration_dict,
            model_checkpoint_location=model_checkpoint_location,
        )
        self._results: Dict[str, List[Dict[str, Any]]] = {}
        self.second_model_type_name = second_model_type_name
        self.second_model_configuration_dict = second_model_configuration_dict
        self.second_model_checkpoint_location = second_model_checkpoint_location

    def get_environment_variables(self) -> Dict[str, str]:
        """Get the environment variables for the job.

        Returns:
            A dictionary containing the environment variables.
        """
        env_variables = {
            "IMAGE_LOCATIONS": json.dumps(
                [location.to_dict() for location in self.image_locations]
            ),
            "MODEL_TYPE_NAME": self.model_type_name,
            "MODEL_CONFIGURATION_DICT": TupleEncoder().encode(
                self.model_configuration_dict
            ),
            "MODEL_CHECKPOINT_LOCATION": json.dumps(
                self.model_checkpoint_location.to_dict()
            ),
            "JOB_TYPE": "PREDICTION",
        }

        if (
            self.second_model_type_name
            and self.second_model_configuration_dict
            and self.second_model_checkpoint_location
        ):
            env_variables["SECOND_MODEL_TYPE_NAME"] = self.second_model_type_name
            env_variables["SECOND_MODEL_CONFIGURATION_DICT"] = TupleEncoder().encode(
                self.second_model_configuration_dict
            )
            env_variables["SECOND_MODEL_CHECKPOINT_LOCATION"] = json.dumps(
                self.second_model_checkpoint_location.to_dict()
            )

        return env_variables
