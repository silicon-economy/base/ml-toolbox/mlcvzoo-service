# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Encapsulates a single training job in the mlcvzoo-service.
"""

from __future__ import annotations

import json
import logging
from pathlib import Path
from typing import Any, Dict, List, Optional

from config_builder.json_encoding import TupleEncoder
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service.services.jobs.base.base_job import BaseJob

logger = logging.getLogger(__name__)


class TrainingJob(BaseJob):
    """
    Encapsulates a single training job in the mlcvzoo-service.
    """

    __training_output_dir_key__: str = "TRAINING_OUTPUT_DIR"
    __tensorboard_dir_key__: str = "TENSORBOARD_DIR"

    def __init__(
        self,
        image_locations: List[S3DataLocation],
        annotation_locations: List[S3DataLocation],
        model_checkpoint_location: S3DataLocation,
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        mlflow_credentials: MlflowCredentials,
        evaluator_configuration_dict: Optional[Dict[str, Any]] = None,
        result_model_checkpoint_location: Optional[S3DataLocation] = None,
    ):
        BaseJob.__init__(
            self,
            image_locations=image_locations,
            model_type_name=model_type_name,
            model_configuration_dict=model_configuration_dict,
            model_checkpoint_location=model_checkpoint_location,
        )
        self.evaluator_configuration_dict = evaluator_configuration_dict
        self.annotation_locations: List[S3DataLocation] = annotation_locations
        self.initial_model_checkpoint_location: S3DataLocation = (
            model_checkpoint_location
        )
        self.result_model_checkpoint_location: Optional[
            S3DataLocation
        ] = result_model_checkpoint_location

        self.mlflow_credentials: MlflowCredentials = mlflow_credentials

        self.__training_data_dir: Path
        self.model_type_name = model_type_name
        self.string_replacement_map: Dict[str, str] = {}

    def get_environment_variables(self) -> Dict[str, str]:
        """Get the environment variables for the job.

        Returns:
            A dictionary containing the environment variables.
        """
        if self.evaluator_configuration_dict:
            return {
                "IMAGE_LOCATIONS": json.dumps(
                    [location.to_dict() for location in self.image_locations]
                ),
                "ANNOTATION_LOCATIONS": json.dumps(
                    [location.to_dict() for location in self.annotation_locations]
                ),
                "MODEL_TYPE_NAME": self.model_type_name,
                "MODEL_CONFIGURATION_DICT": TupleEncoder().encode(
                    self.model_configuration_dict
                ),
                "MODEL_CHECKPOINT_LOCATION": json.dumps(
                    self.model_checkpoint_location.to_dict()
                ),
                "EVAL_CONFIGURATION_DICT": TupleEncoder().encode(
                    self.evaluator_configuration_dict
                ),
                "JOB_TYPE": "TRAINING",
                "MLFLOW_CREDENTIALS": json.dumps(self.mlflow_credentials.to_dict()),
            }

        return {
            "IMAGE_LOCATIONS": json.dumps(
                [location.to_dict() for location in self.image_locations]
            ),
            "ANNOTATION_LOCATIONS": json.dumps(
                [location.to_dict() for location in self.annotation_locations]
            ),
            "MODEL_TYPE_NAME": self.model_type_name,
            "MODEL_CONFIGURATION_DICT": TupleEncoder().encode(
                self.model_configuration_dict
            ),
            "MODEL_CHECKPOINT_LOCATION": json.dumps(
                self.model_checkpoint_location.to_dict()
            ),
            "JOB_TYPE": "TRAINING",
            "MLFLOW_CREDENTIALS": json.dumps(self.mlflow_credentials.to_dict()),
        }
