# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Module defining a service to interact with docker.
"""

import logging
import os
from typing import Union

import docker
from docker.types import DeviceRequest
from mlcvzoo_base.configuration.utils import str2bool

from mlcvzoo_service.services.jobs.prediction.prediction_job import PredictionJob
from mlcvzoo_service.services.jobs.training.training_job import TrainingJob

logger = logging.getLogger(__name__)


class _DockerService:
    """Interact with the docker daemon."""

    def __init__(self, client: docker.DockerClient = docker.from_env()):
        """Initialize a DockerService."""

        self.__client = client

    @staticmethod
    def __load_runtime_image_name(env_variable: str) -> str:
        """Load the runtime image name.

        Args:
            env_variable: The name of the env variable which stores the runtime image name.
        """
        image_name = os.getenv(env_variable)
        if not image_name:
            raise RuntimeError(
                f"Environment variable {env_variable} not set."
                f"Please set it to the runtime image name."
            )
        return image_name

    def get_container_logs(self, container_id: str) -> str:
        """Get the logs of a container.

        Args:
            container_id: The ID of the container to get the logs from.

        Returns:
            The logs of the container.
        """
        container = self.__client.containers.get(container_id)
        return str(container.logs().decode("utf-8"))

    def start_container(
        self, job: Union[PredictionJob, TrainingJob], service_endpoint: str
    ) -> str:
        """Start a container.

        Args:
            job: The job to start a container for.
            service_endpoint: The endpoint of the service to communicate the job finish.

        Returns:
            The ID of the created container.
        """
        if job.model_type_name == "yolox":
            runtime_image = self.__load_runtime_image_name(
                env_variable="YOLOX_RUNTIME_IMAGE"
            )
        elif job.model_type_name == "mmpretrain":
            runtime_image = self.__load_runtime_image_name(
                env_variable="MMPRETRAIN_RUNTIME_IMAGE"
            )
        elif job.model_type_name == "mmdetection_object_detection":
            runtime_image = self.__load_runtime_image_name(
                env_variable="MMDETECTION_RUNTIME_IMAGE"
            )
        elif job.model_type_name == "mmdetection_segmentation":
            runtime_image = self.__load_runtime_image_name(
                env_variable="MMDETECTION_RUNTIME_IMAGE"
            )

        elif job.model_type_name == "mmrotate":
            runtime_image = self.__load_runtime_image_name(
                env_variable="MMROTATE_RUNTIME_IMAGE"
            )

        elif job.model_type_name == "mmocr_text_recognition":
            runtime_image = self.__load_runtime_image_name(
                env_variable="MMOCR_RUNTIME_IMAGE"
            )

        elif job.model_type_name == "mmocr_text_detection":
            runtime_image = self.__load_runtime_image_name(
                env_variable="MMOCR_RUNTIME_IMAGE"
            )

        else:
            raise RuntimeError(f"Model {job.model_type_name} not implemented!")

        job_variables = job.get_environment_variables()
        # Endpoint of the mlcvzoo-service REST
        job_variables["SERVICE_ENDPOINT"] = service_endpoint
        # For local development and testing use NETWORK_MODE=host
        network_mode = os.getenv("NETWORK_MODE", "bridge")
        # TODO: Don't allow all values
        _auto_remove = os.getenv("AUTO_REMOVE_CONTAINER")
        if _auto_remove is not None:
            auto_remove: bool = str2bool(_auto_remove)
        else:
            auto_remove = True

        container = self.__client.containers.run(
            image=runtime_image,
            environment=job_variables,
            detach=True,
            network_mode=network_mode,
            auto_remove=auto_remove,
            entrypoint=[
                "python3",
                "/srv/mlcvzoo-service-jobs/mlcvzoo_service_jobs/main.py",
            ],
            device_requests=[
                DeviceRequest(
                    device_ids=[str(job.gpu)],
                    capabilities=[["gpu"]],
                )
            ],
        )
        return str(container.id)


global_docker_service = _DockerService()
