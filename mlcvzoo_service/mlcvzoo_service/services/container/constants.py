# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Define container service related constants."""

from enum import Enum


class ImplementedContainerServices(Enum):
    """Implemented container services."""

    DOCKER = "DOCKER"
