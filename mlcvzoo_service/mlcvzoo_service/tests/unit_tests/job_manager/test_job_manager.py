# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the JobManager."""
import os
import sys
from unittest.mock import MagicMock

import pytest

from mlcvzoo_service.services.jobs.constants import JobStatus
from mlcvzoo_service.services.jobs.prediction.prediction_job import PredictionJob


@pytest.fixture(name="docker_service")
def docker_service(mocker):
    """Fixture for the DockerService interface."""
    mocker.patch(
        "mlcvzoo_service.services.container.docker.service._DockerService.start_container",
        return_value="1a2b3c",
    )


@pytest.fixture(name="read_open_mock")
def read_open_mock(mocker):
    """Fixture for the open function."""
    return mocker.patch(
        "builtins.open",
        mocker.mock_open(read_data="This is a really long training log."),
    )


@pytest.fixture(name="docker_service_error")
def docker_service_error(mocker):
    """Fixture for the DockerService interface."""
    mocker.patch(
        "mlcvzoo_service.services.container.docker.service._DockerService.start_container",
        side_effect=RuntimeError("Docker error"),
    )


@pytest.fixture(name="get_all_jobs")
def all_jobs_fixture(
    get_running_jobs,
    get_finished_jobs,
    get_crashed_jobs,
    get_queued_jobs,
):
    """Fixture for all jobs."""
    return get_queued_jobs + get_finished_jobs + get_crashed_jobs + get_running_jobs


@pytest.fixture(name="get_single_job")
def single_job_fixture(request):
    """Fixture for a single job."""
    job = MagicMock(spec=PredictionJob)
    job.id = request.param
    job.status = JobStatus.NEW
    job.results = None
    job.gpu = None
    job.container_id = None
    return job


@pytest.fixture(name="get_multiple_jobs")
def multiple_jobs_fixture(request):
    """Fixture for multiple jobs."""
    jobs = []
    for i in range(int(request.param)):
        job = MagicMock(spec=PredictionJob)
        job.id = i
        job.status = JobStatus.NEW
        job.results = None
        job.gpu = None
        job.container_id = None
        jobs.append(job)
    return jobs


@pytest.fixture(name="job_manager")
def job_manager_fixture(
    create_app,
    # docker_service,
    get_running_jobs,
    get_finished_jobs,
    get_crashed_jobs,
    get_queued_jobs,
):
    """JobManager fixture."""
    from mlcvzoo_service.services.job_manager.job_manager import (  # import here so that;;;;;;; create_app fixture is used before
        _JobManager,
    )

    with create_app.app_context():
        manager = _JobManager()
        manager._crashed_jobs = get_crashed_jobs
        manager._queued_jobs = get_queued_jobs
        manager._finished_jobs = get_finished_jobs

        running_jobs = {i: job for i, job in enumerate(get_running_jobs)}
        manager._running_jobs = running_jobs

        return manager


@pytest.fixture(name="job_manager_empty")
def job_manager_empty_fixture(create_app):
    """Empty JobManager fixture

    Empty means there are no jobs in the JobManager.
    ."""
    with create_app.app_context():
        from mlcvzoo_service.services.job_manager.job_manager import _JobManager

        return _JobManager()


def test_job_manager_running_jobs_property(job_manager, get_running_jobs):
    """Test the running_jobs property of the JobManager."""
    assert len(job_manager.running_jobs) == 2
    assert (
        type(job_manager.running_jobs) is list
    )  # check because the attribute is a dict and is converted for the property
    assert job_manager.running_jobs[0].id == get_running_jobs[0].id
    assert job_manager.running_jobs[0].status == get_running_jobs[0].status
    assert job_manager.running_jobs[1].id == get_running_jobs[1].id
    assert job_manager.running_jobs[1].status == get_running_jobs[1].status


def test_job_manager_finished_jobs_property(job_manager, get_finished_jobs):
    """Test the finished_jobs property of the JobManager."""
    assert len(job_manager.finished_jobs) == 2
    assert job_manager.finished_jobs[0].id == get_finished_jobs[0].id
    assert job_manager.finished_jobs[0].status == get_finished_jobs[0].status
    assert job_manager.finished_jobs[1].id == get_finished_jobs[1].id
    assert job_manager.finished_jobs[1].status == get_finished_jobs[1].status


def test_job_manager_crashed_jobs_property(job_manager, get_crashed_jobs):
    """Test the crashed_jobs property of the JobManager."""
    assert len(job_manager.crashed_jobs) == 2
    assert job_manager.crashed_jobs[0].id == get_crashed_jobs[0].id
    assert job_manager.crashed_jobs[0].status == get_crashed_jobs[0].status
    assert job_manager.crashed_jobs[1].id == get_crashed_jobs[1].id
    assert job_manager.crashed_jobs[1].status == get_crashed_jobs[1].status


def test_job_manager_queued_jobs_property(job_manager, get_queued_jobs):
    """Test the queued_jobs property of the JobManager."""
    assert len(job_manager.queued_jobs) == 2
    assert job_manager.queued_jobs[0].id == get_queued_jobs[0].id
    assert job_manager.queued_jobs[0].status == get_queued_jobs[0].status
    assert job_manager.queued_jobs[1].id == get_queued_jobs[1].id
    assert job_manager.queued_jobs[1].status == get_queued_jobs[1].status


def test_job_manager_all_jobs_property(job_manager):
    """Test the all_jobs property of the JobManager.

    Since this is a property that combines all the other properties, we can test it by checking
    if the length of the
    property is the sum of the lengths of the other properties.
    """
    # with create_app.app_context():
    assert len(job_manager.all_jobs) == 8


def test_job_manager_gpu_map_property(job_manager):
    """Test the gpu_map property of the JobManager."""
    # check set_environment_variables fixture in common_fixtures; only GPU with ID 1 is set
    assert job_manager.gpu_map == {1: None}


def test_get_job(job_manager, get_all_jobs):
    """Test the get_job method of the JobManager."""
    for job in get_all_jobs:  # check all jobs
        assert job_manager.get_job(job.id).container_id == job.container_id
        assert job_manager.get_job(job.id).status.value == job.status.value


def test_get_job_not_found(job_manager):
    """Test the get_job method of the JobManager when the job is not found."""
    job = job_manager.get_job("42")
    assert job is None


def test_get_job_status(job_manager, get_all_jobs):
    """Test the get_job_status method of the JobManager."""
    for job in get_all_jobs:  # check all jobs
        status, results = job_manager.get_job_status(job.id)
        assert status == job.status
        assert results == job.results


def test_get_job_status_not_found(job_manager):
    """Test the get_job_status method of the JobManager when the job is not found."""
    status, results = job_manager.get_job_status("42")
    assert status is None
    assert results is None


@pytest.mark.parametrize("get_single_job", ["42"], indirect=True)
def test_add_job_empty_queue(job_manager_empty, get_single_job, docker_service):
    """Test the add_job method of the JobManager when the queue is empty.

    Expected behaviour:
        - The job is added to running_jobs
        - The job's status is set to RUNNING
        - The job's container_id is set to the container_id returned by the DockerService
        - The job's gpu is set to the first available GPU
    """
    job = get_single_job
    manager = job_manager_empty
    manager.add_job(job)

    # check job lists
    assert len(manager.running_jobs) == 1
    assert len(manager.all_jobs) == 1

    # check job
    assert job.status.value == "RUNNING"
    assert job.container_id == "1a2b3c"  # from docker_service fixture
    assert job.gpu == 1  # from set_environment_variables fixture


@pytest.mark.parametrize("get_multiple_jobs", ["2"], indirect=True)
def test_add_job_no_gpu_available(job_manager_empty, get_multiple_jobs, docker_service):
    """Test the add_job method of the JobManager when no gpu is available.

    Expected behaviour:
        - Blocking job is added to running_jobs (expected behaviour as in previous test)
        - The job is added to queued_jobs
        - The job's status is set to QUEUED
        - The job's container_id is None
        - The job's gpu is None
    """
    blocking_job, job_2 = get_multiple_jobs

    # add first job to block gpu availability
    manager = job_manager_empty
    manager.add_job(blocking_job)

    # add second job
    manager.add_job(job_2)

    # check job lists
    assert len(manager.queued_jobs) == 1
    assert len(manager.running_jobs) == 1
    assert len(manager.all_jobs) == 2

    # check blocking_job
    assert blocking_job.status.value == "RUNNING"
    assert blocking_job.container_id == "1a2b3c"  # from docker_service fixture
    assert blocking_job.gpu == 1  # from set_environment_variables fixture

    # check job_2
    assert job_2.status.value == "NEW"
    assert job_2.container_id is None
    assert job_2.gpu is None


@pytest.mark.parametrize("get_single_job", ["42"], indirect=True)
def test_add_job_error(job_manager_empty, get_single_job, docker_service_error):
    """Test the add_job method of the JobManager when the start of the container fails.

    Expected behaviour:
        - An RuntimeError is raised
        - No job is added to the queue or the running jobs
    """
    job = get_single_job
    manager = job_manager_empty
    with pytest.raises(RuntimeError):
        manager.add_job(job)

        # check job lists
        assert len(manager.running_jobs) == 0
        assert len(manager.queued_jobs) == 0


@pytest.mark.parametrize("get_single_job", ["42"], indirect=True)
def test_finish_job(job_manager_empty, get_single_job, read_open_mock):
    """Test the finish_job method of the JobManager with no job in queue.

    Expected behaviour:
        - The job is added to finished_jobs
        - The job's status is set to FINISHED
        - The job's results are set to the results passed to the method
        - The job's gpu is None
        - The gpu map is reset
    """
    # arrange job and manager
    job = get_single_job
    job.status = JobStatus.RUNNING
    job.gpu = 1
    job.container_id = "1a2b3c"

    manager = job_manager_empty
    manager._running_jobs = {job.id: job}
    manager._gpu_map = {1: job.id}

    results = {"result": "result"}
    manager.finish_job(job.id, results)

    # check job lists
    assert len(manager.finished_jobs) == 1
    assert len(manager.all_jobs) == 1

    # check job
    assert job.status.value == "FINISHED"
    assert job.results == results
    assert job.gpu is None
    assert manager.gpu_map == {1: None}


@pytest.mark.parametrize("get_multiple_jobs", ["2"], indirect=True)
def test_finish_job_with_job_in_queue(
    job_manager_empty, get_multiple_jobs, docker_service, read_open_mock
):
    """Test the finish_job method of the JobManager with one job in queue.

    Expected behaviour:
        - Expected behaviour for the first job as in the previous test
        - The second job is added to running_jobs
        - The second job's status is set to RUNNING
        - The second job's container_id is set to the container_id returned by the DockerService
        - The second job's gpu is set to the first available GPU
        - The gpu map is updated
    """
    # arrange jobs and manager
    first_job, second_job = get_multiple_jobs

    first_job.status = JobStatus.RUNNING
    first_job.gpu = 1
    first_job.container_id = "1a2b3c"

    manager = job_manager_empty
    manager._running_jobs = {first_job.id: first_job}
    manager._gpu_map = {1: first_job.id}
    manager._queued_jobs.append(second_job)

    # finish blocking job
    results = {"result": "result"}
    manager.finish_job(first_job.id, results)

    # check job lists
    assert len(manager.running_jobs) == 1
    assert len(manager.finished_jobs) == 1
    assert len(manager.all_jobs) == 2

    # check blocking_job
    assert first_job.status.value == "FINISHED"
    assert first_job.results == results
    assert first_job.gpu is None

    # check gpu map
    assert manager.gpu_map == {1: second_job}

    # check job_2
    assert second_job.status.value == "RUNNING"
    assert second_job.container_id == "1a2b3c"


@pytest.mark.parametrize("get_single_job", ["42"], indirect=True)
def test_finish_job_error(job_manager_empty, get_single_job):
    """Test the finish_job method of the JobManager when the job is not found.

    Expected behaviour:
        - A RuntimeError is raised
    """
    # arrange job and manager
    job = get_single_job
    manager = job_manager_empty

    with pytest.raises(RuntimeError):
        manager.finish_job(job.id, {})


@pytest.mark.parametrize("get_single_job", ["42"], indirect=True)
def test_finish_job_crashed(job_manager_empty, get_single_job, read_open_mock):
    """Test the finish_job method of the JobManager when the job is crashed.

    Expected behaviour:
        - The job is added to crashed_jobs
        - The job's status is set to CRASHED
        - The job's results are set to the results passed to the method
        - The job's gpu is None
        - The gpu map is reset
    """
    # arrange job and manager
    job = get_single_job
    job.status = JobStatus.RUNNING
    job.gpu = 1
    job.container_id = "1a2b3c"

    manager = job_manager_empty
    manager._running_jobs = {job.id: job}
    manager._gpu_map = {1: job.id}

    results = {"crashed": True, "message": "The test job crashed"}
    manager.finish_job(job.id, results)

    # check job lists
    assert len(manager.crashed_jobs) == 1
    assert len(manager.all_jobs) == 1

    # check job
    assert job.status.value == "CRASHED"
    assert job.results == {"message": "The test job crashed"}
    assert job.gpu is None
    assert manager.gpu_map == {1: None}


@pytest.mark.parametrize("get_single_job", ["42"], indirect=True)
def test_finish_job_no_gpu(job_manager_empty, get_single_job, read_open_mock):
    """Test the finish_job method of the JobManager when the job is has no gpu attached.

    Expected behaviour:
        - A runtime error is raised indicating that the job manager has an inconsistent state
    """
    # arrange job and manager
    job = get_single_job
    job.status = JobStatus.RUNNING
    job.gpu = None

    manager = job_manager_empty
    manager._running_jobs = {job.id: job}
    manager._gpu_map = {1: job.id}

    results = {"result": "result"}

    with pytest.raises(RuntimeError):
        manager.finish_job(job.id, results)


def test_read_job_logs(job_manager, read_open_mock):
    """Test the read_job_logs method of the JobManager.

    Expected behaviour:
        - The logs of the container are read
    """
    job = MagicMock()
    job.container_id = "42"
    job_manager._running_jobs = {"42": job}

    logs = job_manager.read_job_logs("42")
    assert logs == "This is a really long training log."  # check read_open_mock fixture

    expected_log_file_path = "/".join(
        read_open_mock.call_args[0][0].split("/")[-2:]
    )  # we do not care about the full path but the
    # relative path of the job_log_dir

    assert expected_log_file_path == "job_logs/42.txt"


def test_read_job_logs_no_file_error(job_manager):
    """Test if a FileNotFoundError is caught if there are no logs saved for the given job.

    Expected behaviour:
        - An empty string is returned.
    """
    job = MagicMock()
    job.container_id = "42"
    job_manager._running_jobs = {"42": job}

    logs = job_manager.read_job_logs("42")
    assert logs == ""


def test_job_manager_init_error_no_gpus_set():
    """Test the JobManager when no GPUs are set.

    Expected behaviour:
        - A RuntimeError is raised
    """
    os.environ["SERVICE_ENDPOINT"] = "test-endpoint"
    if "GPUS" in os.environ:
        del os.environ["GPUS"]

    with pytest.raises(RuntimeError):
        from mlcvzoo_service.services.job_manager.job_manager import _JobManager

        manager = _JobManager()

    del os.environ["SERVICE_ENDPOINT"]


def test_job_manager_init_error_no_endpoint_set():
    """Test the JobManager when no service endpoint is set.

    Expected behaviour:
        - A RuntimeError is raised
    """
    os.environ["GPUS"] = "0,1"
    if "SERVICE_ENDPOINT" in os.environ:
        del os.environ["SERVICE_ENDPOINT"]

    with pytest.raises(RuntimeError):
        from mlcvzoo_service.services.job_manager.job_manager import _JobManager

        manager = _JobManager()

    del os.environ["GPUS"]


def test_job_manager_init_error_wrong_container_service_set():
    """Test the JobManager when a unsupported container service is set.

    Expected behaviour:
        - A RuntimeError is raised
    """
    os.environ["GPUS"] = "0,1"
    os.environ["SERVICE_ENDPOINT"] = "test-endpoint"
    os.environ["CONTAINER_SERVICE"] = "NON_EXISTING_SERVICE"

    with pytest.raises(ValueError):
        from mlcvzoo_service.services.job_manager.job_manager import _JobManager

        manager = _JobManager()

    del os.environ["GPUS"]
    del os.environ["SERVICE_ENDPOINT"]
    del os.environ["CONTAINER_SERVICE"]
