# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the Docker service."""
import os
from unittest.mock import MagicMock

import docker
import pytest
from docker.types import DeviceRequest
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service.services.jobs.prediction.prediction_job import PredictionJob
from mlcvzoo_service.tests.unit_tests.test_utils.utils import TestConfig


@pytest.fixture(name="get_job_mock")
def get_job_mock(request, s3_credentials):
    """Get a PredictionJob. As the DockerService does not distinguish between training and
    prediction jobs, PredictionJob is used as it needs less code to mock."""
    model_type = request.param

    test_job = PredictionJob(
        image_locations=[
            S3DataLocation(
                uri="https://127.0.0.1:9000/test-bucket/images",
                location_id="TEST_OUTPUT",
                credentials=s3_credentials,
            )
        ],
        model_checkpoint_location=S3DataLocation(
            uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
            location_id="TEST_OUTPUT",
            credentials=s3_credentials,
        ),
        model_type_name=model_type,
        model_configuration_dict=TestConfig(
            unique_name=f"{model_type}-test_model"
        ).to_dict(),
    )
    test_job.gpu = 0
    return test_job


@pytest.fixture(name="docker_env_variable")
def docker_env_variable_fixture():
    """Set the environment variable to use the DockerService."""
    yolox_image = os.getenv("YOLOX_RUNTIME_IMAGE")
    if yolox_image is None:
        os.environ["YOLOX_RUNTIME_IMAGE"] = "yolox_image"

    mmdet_image = os.getenv("MMDETECTION_RUNTIME_IMAGE")
    if mmdet_image is None:
        os.environ["MMDETECTION_RUNTIME_IMAGE"] = "mmdetection_image"

    mmpretrain_image = os.getenv("MMPRETRAIN_RUNTIME_IMAGE")
    if mmpretrain_image is None:
        os.environ["MMPRETRAIN_RUNTIME_IMAGE"] = "mmpretrain_image"

    mmdetection_segmentation_image = os.getenv("MMDETECTION_RUNTIME_IMAGE")
    if mmdetection_segmentation_image is None:
        os.environ["MMDETECTION_RUNTIME_IMAGE"] = "mmdetection_segmentation_image"

    mmrotate_image = os.getenv("MMROTATE_RUNTIME_IMAGE")
    if mmrotate_image is None:
        os.environ["MMROTATE_RUNTIME_IMAGE"] = "mmrotate_image"

    mmocr_text_recognition_image = os.getenv("MMOCR_RUNTIME_IMAGE")
    # TODO: How to unset env vars with pytest, setting as same val for now
    if mmocr_text_recognition_image is None:
        os.environ[
            "MMOCR_RUNTIME_IMAGE"
        ] = "mmocr_text_image"  # "mmocr_text_recognition_image"

    mmocr_text_detection_image = os.getenv("MMOCR_RUNTIME_IMAGE")
    if mmocr_text_detection_image is None:
        os.environ[
            "MMOCR_RUNTIME_IMAGE"
        ] = "mmocr_text_image"  # "mmocr_text_detection_image"


@pytest.fixture(name="docker_service")
def docker_service_fixture(docker_fixture, docker_env_variable):
    """Get a DockerService."""
    from mlcvzoo_service.services.container.docker.service import (  # import here so;; docker_fixture is used before
        _DockerService,
    )

    client = MagicMock(spec=docker.DockerClient)
    container = MagicMock()
    container.id = "1a2b3c"
    client.containers.run.return_value = container
    client.containers.get.return_value.logs.return_value = b"Test logs"
    return _DockerService(client=client)


@pytest.fixture(name="get_device_request")
def get_device_request():
    """Get a device request."""
    return DeviceRequest(
        device_ids=["0"],
        capabilities=[["gpu"]],
    )


# TODO: parametrize _start_container test in one test class
@pytest.mark.parametrize("get_job_mock", ["yolox"], indirect=True)
def test_start_container_yolox(get_job_mock, docker_service, get_device_request):
    """Test if a container is started for a YOLOX job."""
    mock_job = get_job_mock
    service = docker_service

    container_id = service.start_container(
        job=mock_job, service_endpoint="http://test-endpoint"
    )

    # expected values
    device_request = get_device_request
    env_variables = mock_job.get_environment_variables()
    env_variables["SERVICE_ENDPOINT"] = "http://test-endpoint"

    assert container_id == "1a2b3c"
    assert len(service._DockerService__client.containers.run.call_args_list) == 1
    assert service._DockerService__client.containers.run.call_args_list[0].kwargs == {
        "image": "yolox_image",  # check docker_env_variable fixture for the image name
        "environment": env_variables,
        "network_mode": "bridge",  # default if not configured via env variable otherwise,
        "device_requests": [device_request],
        "detach": True,
        "auto_remove": True,
        "entrypoint": [
            "python3",
            "/srv/mlcvzoo-service-jobs/mlcvzoo_service_jobs/main.py",
        ],
    }


@pytest.mark.parametrize(
    "get_job_mock", ["mmdetection_object_detection"], indirect=True
)
def test_start_container_mmdetection(get_job_mock, docker_service, get_device_request):
    """Test if a container is started for an MMDetection job."""
    mock_job = get_job_mock
    service = docker_service

    container_id = service.start_container(
        job=mock_job, service_endpoint="http://test-endpoint"
    )

    # expected values
    device_request = get_device_request
    env_variables = mock_job.get_environment_variables()
    env_variables["SERVICE_ENDPOINT"] = "http://test-endpoint"

    assert container_id == "1a2b3c"
    assert len(service._DockerService__client.containers.run.call_args_list) == 1
    assert service._DockerService__client.containers.run.call_args_list[0].kwargs == {
        "image": "mmdetection_image",
        # check docker_env_variable fixture for the image name
        "environment": env_variables,
        "network_mode": "bridge",  # default if not configured via env variable otherwise,
        "device_requests": [device_request],
        "detach": True,
        "auto_remove": True,
        "entrypoint": [
            "python3",
            "/srv/mlcvzoo-service-jobs/mlcvzoo_service_jobs/main.py",
        ],
    }


@pytest.mark.parametrize("get_job_mock", ["mmdetection_segmentation"], indirect=True)
def test_start_container_mmdetection_segmentation(
    get_job_mock, docker_service, get_device_request
):
    """Test if a container is started for an MMDetectionSegmentation job."""
    mock_job = get_job_mock
    service = docker_service

    container_id = service.start_container(
        job=mock_job, service_endpoint="http://test-endpoint"
    )

    # expected values
    device_request = get_device_request
    env_variables = mock_job.get_environment_variables()
    env_variables["SERVICE_ENDPOINT"] = "http://test-endpoint"

    assert container_id == "1a2b3c"
    assert len(service._DockerService__client.containers.run.call_args_list) == 1
    assert service._DockerService__client.containers.run.call_args_list[0].kwargs == {
        "image": "mmdetection_image",
        # check docker_env_variable fixture for the image name
        "environment": env_variables,
        "network_mode": "bridge",  # default if not configured via env variable otherwise,
        "device_requests": [device_request],
        "detach": True,
        "auto_remove": True,
        "entrypoint": [
            "python3",
            "/srv/mlcvzoo-service-jobs/mlcvzoo_service_jobs/main.py",
        ],
    }


@pytest.mark.parametrize("get_job_mock", ["mmpretrain"], indirect=True)
def test_start_container_mmpretrain(get_job_mock, docker_service, get_device_request):
    """Test if a container is started for an MMPretrain job."""
    mock_job = get_job_mock
    service = docker_service

    container_id = service.start_container(
        job=mock_job, service_endpoint="http://test-endpoint"
    )

    # expected values
    device_request = get_device_request
    env_variables = mock_job.get_environment_variables()
    env_variables["SERVICE_ENDPOINT"] = "http://test-endpoint"

    assert container_id == "1a2b3c"
    assert len(service._DockerService__client.containers.run.call_args_list) == 1
    assert service._DockerService__client.containers.run.call_args_list[0].kwargs == {
        "image": "mmpretrain_image",
        # check docker_env_variable fixture for the image name
        "environment": env_variables,
        "network_mode": "bridge",  # default if not configured via env variable otherwise,
        "device_requests": [device_request],
        "detach": True,
        "auto_remove": True,
        "entrypoint": [
            "python3",
            "/srv/mlcvzoo-service-jobs/mlcvzoo_service_jobs/main.py",
        ],
    }


@pytest.mark.parametrize("get_job_mock", ["mmrotate"], indirect=True)
def test_start_container_mmrotate(get_job_mock, docker_service, get_device_request):
    """Test if a container is started for an MMPretrain job."""
    mock_job = get_job_mock
    service = docker_service

    container_id = service.start_container(
        job=mock_job, service_endpoint="http://test-endpoint"
    )

    # expected values
    device_request = get_device_request
    env_variables = mock_job.get_environment_variables()
    env_variables["SERVICE_ENDPOINT"] = "http://test-endpoint"

    assert container_id == "1a2b3c"
    assert len(service._DockerService__client.containers.run.call_args_list) == 1
    assert service._DockerService__client.containers.run.call_args_list[0].kwargs == {
        "image": "mmrotate_image",
        # check docker_env_variable fixture for the image name
        "environment": env_variables,
        "network_mode": "bridge",  # default if not configured via env variable otherwise,
        "device_requests": [device_request],
        "detach": True,
        "auto_remove": True,
        "entrypoint": [
            "python3",
            "/srv/mlcvzoo-service-jobs/mlcvzoo_service_jobs/main.py",
        ],
    }


@pytest.mark.parametrize("get_job_mock", ["mmocr_text_detection"], indirect=True)
def test_start_container_mmocr_text_detection(
    get_job_mock, docker_service, get_device_request
):
    """Test if a container is started for an MMPretrain job."""
    mock_job = get_job_mock
    service = docker_service

    container_id = service.start_container(
        job=mock_job, service_endpoint="http://test-endpoint"
    )

    # expected values
    device_request = get_device_request
    env_variables = mock_job.get_environment_variables()
    env_variables["SERVICE_ENDPOINT"] = "http://test-endpoint"

    assert container_id == "1a2b3c"
    assert len(service._DockerService__client.containers.run.call_args_list) == 1
    assert service._DockerService__client.containers.run.call_args_list[0].kwargs == {
        "image": "mmocr_text_image",  # "mmocr_text_detection_image",
        # check docker_env_variable fixture for the image name
        "environment": env_variables,
        "network_mode": "bridge",  # default if not configured via env variable otherwise,
        "device_requests": [device_request],
        "detach": True,
        "auto_remove": True,
        "entrypoint": [
            "python3",
            "/srv/mlcvzoo-service-jobs/mlcvzoo_service_jobs/main.py",
        ],
    }


@pytest.mark.parametrize("get_job_mock", ["mmocr_text_recognition"], indirect=True)
def test_start_container_mmocr_text_recognition(
    get_job_mock, docker_service, get_device_request
):
    """Test if a container is started for an MMPretrain job."""
    mock_job = get_job_mock
    service = docker_service

    container_id = service.start_container(
        job=mock_job, service_endpoint="http://test-endpoint"
    )

    # expected values
    device_request = get_device_request
    env_variables = mock_job.get_environment_variables()
    env_variables["SERVICE_ENDPOINT"] = "http://test-endpoint"

    assert container_id == "1a2b3c"
    assert len(service._DockerService__client.containers.run.call_args_list) == 1
    assert service._DockerService__client.containers.run.call_args_list[0].kwargs == {
        "image": "mmocr_text_image",  # "mmocr_text_recognition_image",
        # check docker_env_variable fixture for the image name
        "environment": env_variables,
        "network_mode": "bridge",  # default if not configured via env variable otherwise,
        "device_requests": [device_request],
        "detach": True,
        "auto_remove": True,
        "entrypoint": [
            "python3",
            "/srv/mlcvzoo-service-jobs/mlcvzoo_service_jobs/main.py",
        ],
    }


@pytest.mark.parametrize(
    "get_job_mock", ["not_implemented_framework", None], indirect=True
)
def test_start_container_not_implemented(
    get_job_mock, docker_service, get_device_request
):
    """Test if a RuntimeError is raised if a not implemented framework is used."""
    mock_job = get_job_mock
    service = docker_service

    with pytest.raises(RuntimeError):
        container_id = service.start_container(
            job=mock_job, service_endpoint="http://test-endpoint"
        )


def test_get_container_logs(docker_service):
    """Test if the logs of a container are returned."""
    service = docker_service
    container_id = "1a2b3c"

    logs = service.get_container_logs(container_id=container_id)

    assert logs == "Test logs"
