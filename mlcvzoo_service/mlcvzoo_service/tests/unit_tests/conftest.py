# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Define common fixtures for the unit tests."""

import os
from logging import getLogger
from pathlib import Path
from unittest.mock import MagicMock

import docker
import pytest
from config_builder.replacement_map import (
    get_current_replacement_map,
    set_replacement_map_value,
    update_replacement_map_from_os,
)
from flask import Flask
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

import mlcvzoo_service
from mlcvzoo_service.services.jobs.constants import JobStatus

logger = getLogger(__name__)


@pytest.fixture(name="set_environment_variables")
def set_environment_variables():
    """Set required environment variables for the tests.

    Since they are needed for the service start up, they are auto used by every test
    """
    endpoint = os.getenv("SERVICE_ENDPOINT")
    if endpoint is None:
        os.environ["SERVICE_ENDPOINT"] = "http://test-endpoint"
    gpus = os.getenv("GPUS")
    if gpus is None:
        os.environ["GPUS"] = "1"


@pytest.fixture(name="docker_fixture")
def docker_client_fixture(mocker):
    """Create a docker client for the tests."""
    with mocker.patch(
        "docker.from_env", return_value=MagicMock(spec=docker.DockerClient)
    ) as mock_client:
        yield mock_client


@pytest.fixture
def create_app(set_environment_variables, docker_fixture) -> Flask:
    """Create a testing app.

    For the setup, some environment variables must be set Therefore, the import is done
    """
    from mlcvzoo_service.server import app

    app.config["TESTING"] = True
    return app


@pytest.fixture(autouse=True)
def set_directory_for_tests():
    """Set the correct directory for the tests."""
    this_dir = Path(os.path.dirname(os.path.abspath(__file__))).resolve()

    setup_path = this_dir
    while setup_path.exists() and setup_path.name != mlcvzoo_service.__name__:
        if setup_path == setup_path.parent:
            raise RuntimeError("Could not find setup_path!")
        else:
            setup_path = setup_path.parent
    # One more to be above the target directory
    setup_path = setup_path.parent

    project_root = str(setup_path)
    code_root = str(setup_path)

    set_replacement_map_value(ReplacementConfig.PROJECT_ROOT_DIR_KEY, project_root)
    update_replacement_map_from_os()
    string_replacement_map = get_current_replacement_map()
    os.chdir(code_root)
    return this_dir, project_root, code_root, string_replacement_map


@pytest.fixture(name="get_all_jobs")
def all_jobs():
    """Get all jobs with different states."""
    job_1 = MagicMock()
    job_1.id = "1"
    job_1.status = JobStatus.RUNNING
    job_1.gpu = "0"
    job_1.container_id = "123"
    job_1.results = {}

    job_2 = MagicMock()
    job_2.id = "2"
    job_2.status = JobStatus.CRASHED
    job_2.gpu = None
    job_2.container_id = "456"
    job_2.results = {}

    job_3 = MagicMock()
    job_3.id = "3"
    job_3.status = JobStatus.FINISHED
    job_3.gpu = None
    job_3.container_id = "789"
    job_3.results = {}

    return [job_1, job_2, job_3]


@pytest.fixture(name="get_running_jobs")
def running_jobs():
    """Get jobs with running state."""
    job = MagicMock()
    job.id = "4"
    job.status = JobStatus.RUNNING
    job.gpu = "0"
    job.container_id = "456"
    job.results = {}

    job_2 = MagicMock()
    job_2.id = "5"
    job_2.status = JobStatus.RUNNING
    job_2.gpu = "1"
    job_2.container_id = "567"
    job_2.results = {}
    return [job, job_2]


@pytest.fixture(name="get_finished_jobs")
def finished_jobs():
    """Get jobs with finished state."""
    job = MagicMock()
    job.id = "6"
    job.status = JobStatus.FINISHED
    job.gpu = None
    job.container_id = "678"
    job.results = {}

    job_2 = MagicMock()
    job_2.id = "7"
    job_2.status = JobStatus.FINISHED
    job_2.gpu = None
    job_2.container_id = "789"
    job_2.results = {}
    return [job, job_2]


@pytest.fixture(name="get_queued_jobs")
def queued_jobs():
    """Get jobs with new state."""
    job = MagicMock()
    job.id = "8"
    job.status = JobStatus.NEW
    job.gpu = None
    job.container_id = "891"
    job.results = {}

    job_2 = MagicMock()
    job_2.id = "9"
    job_2.status = JobStatus.NEW
    job_2.gpu = None
    job_2.container_id = "912"
    job_2.results = {}
    return [job, job_2]


@pytest.fixture(name="get_crashed_jobs")
def crashed_jobs():
    """Get jobs with crashed state."""
    job = MagicMock()
    job.id = "10"
    job.status = JobStatus.CRASHED
    job.gpu = None
    job.container_id = "101"
    job.results = {}

    job_2 = MagicMock()
    job_2.id = "11"
    job_2.status = JobStatus.CRASHED
    job_2.gpu = None
    job_2.container_id = "111"
    job_2.results = {}
    return [job, job_2]


@pytest.fixture(name="s3_credentials")
def s3_credentials():
    """Get S3 credentials."""
    return S3Credentials(
        s3_artifact_access_id="testuser",
        s3_artifact_access_key="123456",
        s3_region_name="eu-central-1",
        s3_endpoint_url="http://test-s3:9000",
    )


@pytest.fixture(name="add_job_mock")
def add_job_mock(mocker, request):
    """Mock the JobManager class with given return value."""
    mocker.patch(
        "mlcvzoo_service.api.routes.prediction.global_job_manager.add_job",
        return_value=request.param,
    )


@pytest.fixture(name="add_job_fail_mock")
def add_job_fail_mock(mocker):
    """Mock the JobManager class with given return value."""
    mocker.patch(
        "mlcvzoo_service.api.routes.prediction.global_job_manager.add_job",
        side_effect=RuntimeError("Job could not be added."),
    )


@pytest.fixture(name="image_locations")
def get_image_locations(s3_credentials):
    """Get test image locations."""
    location_1 = S3DataLocation(
        uri="s3://test_dir/test_image.jpg",
        location_id="MEDIA_DIR",
        credentials=s3_credentials,
    )
    location_2 = S3DataLocation(
        uri="s3://test_dir_2/test_image_2.jpg",
        location_id="MEDIA_DIR",
        credentials=s3_credentials,
    )
    return [location_1, location_2]


@pytest.fixture(name="checkpoint_location")
def get_checkpoint_location(s3_credentials):
    """Get test checkpoint location."""
    return S3DataLocation(
        uri="s3://test_dir/test_checkpoint.pth",
        location_id="MEDIA_DIR",
        credentials=s3_credentials,
    )


@pytest.fixture(name="configuration_dict")
def get_configuration_dict():
    """Get test configuration dictionary."""
    return {
        "test_key": "test_value",
        "test_dict": {"test_nested_key": "test_nested_value"},
    }
