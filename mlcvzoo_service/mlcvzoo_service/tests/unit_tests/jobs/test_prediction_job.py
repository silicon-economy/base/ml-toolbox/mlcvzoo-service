# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the PredictionJob."""
import json

import pytest
from config_builder.json_encoding import TupleEncoder

from mlcvzoo_service.services.jobs.constants import JobStatus
from mlcvzoo_service.services.jobs.prediction.prediction_job import PredictionJob


@pytest.fixture(name="prediction_job")
def prediction_job_fixture(image_locations, checkpoint_location, configuration_dict):
    """Create a prediction job."""
    return PredictionJob(
        image_locations=image_locations,
        model_type_name="test_model",
        model_configuration_dict=configuration_dict,
        model_checkpoint_location=checkpoint_location,
    )


@pytest.fixture(name="prediction_job_with_second_model")
def prediction_job_fixture_second_model(
    image_locations, checkpoint_location, configuration_dict
):
    """Create a prediction job."""
    return PredictionJob(
        image_locations=image_locations,
        model_type_name="test_model",
        model_configuration_dict=configuration_dict,
        model_checkpoint_location=checkpoint_location,
        second_model_checkpoint_location=checkpoint_location,
        second_model_configuration_dict=configuration_dict,
        second_model_type_name="test_model_2",
    )


def test_prediction_job_creation(
    prediction_job, image_locations, checkpoint_location, configuration_dict
):
    """Test the creation of a prediction job."""
    job = prediction_job

    assert job.image_locations == image_locations
    assert job.model_type_name == "test_model"
    assert job.model_configuration_dict == configuration_dict
    assert job.model_checkpoint_location == checkpoint_location
    assert job.results == {}
    assert job.id is not None
    assert job.status == JobStatus.NEW
    assert job.gpu is None
    assert job.container_id is None


def test_prediction_job_creation_with_second_model(
    prediction_job_with_second_model,
    image_locations,
    checkpoint_location,
    configuration_dict,
):
    """Test the creation of a prediction job with optional second model params."""
    job = prediction_job_with_second_model

    assert job.image_locations == image_locations
    assert job.model_type_name == "test_model"
    assert job.model_configuration_dict == configuration_dict
    assert job.model_checkpoint_location == checkpoint_location
    assert job.results == {}
    assert job.id is not None
    assert job.status == JobStatus.NEW
    assert job.gpu is None
    assert job.container_id is None
    assert job.second_model_checkpoint_location == checkpoint_location
    assert job.second_model_configuration_dict == configuration_dict
    assert job.second_model_type_name == "test_model_2"


def test_get_environment_variables(
    prediction_job, image_locations, checkpoint_location, configuration_dict
):
    """Test if the environment variables are correctly returned."""
    environment_variables = prediction_job.get_environment_variables()

    assert json.loads(environment_variables["IMAGE_LOCATIONS"]) == [
        location.to_dict() for location in image_locations
    ]
    assert environment_variables["MODEL_TYPE_NAME"] == "test_model"
    assert (
        json.loads(environment_variables["MODEL_CONFIGURATION_DICT"])
        == configuration_dict
    )
    assert (
        json.loads(environment_variables["MODEL_CHECKPOINT_LOCATION"])
        == checkpoint_location.to_dict()
    )
    assert environment_variables["JOB_TYPE"] == "PREDICTION"


def test_get_environment_variables_with_second_model(
    prediction_job_with_second_model,
    image_locations,
    checkpoint_location,
    configuration_dict,
):
    """Test if the environment variables are correctly returned when second model params are set."""
    environment_variables = prediction_job_with_second_model.get_environment_variables()

    assert json.loads(environment_variables["IMAGE_LOCATIONS"]) == [
        location.to_dict() for location in image_locations
    ]
    assert environment_variables["MODEL_TYPE_NAME"] == "test_model"
    assert (
        json.loads(environment_variables["MODEL_CONFIGURATION_DICT"])
        == configuration_dict
    )
    assert (
        json.loads(environment_variables["MODEL_CHECKPOINT_LOCATION"])
        == checkpoint_location.to_dict()
    )
    assert environment_variables["JOB_TYPE"] == "PREDICTION"
    assert environment_variables["SECOND_MODEL_TYPE_NAME"] == "test_model_2"
    assert (
        json.loads(environment_variables["SECOND_MODEL_CHECKPOINT_LOCATION"])
        == checkpoint_location.to_dict()
    )
    assert (
        json.loads(environment_variables["SECOND_MODEL_CONFIGURATION_DICT"])
        == configuration_dict
    )
