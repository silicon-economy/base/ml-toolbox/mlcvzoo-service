# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the TrainingJob."""
import json

import pytest
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service.services.jobs.constants import JobStatus
from mlcvzoo_service.services.jobs.training.training_job import TrainingJob


@pytest.fixture(name="eval_config")
def get_eval_config():
    """Get test evaluation configuration."""
    return {
        "test_eval_key": "test_value",
        "test_eval_dict": {"test_nested_key": "test_nested_value"},
    }


@pytest.fixture(name="annotation_locations")
def get_annotation_locations(s3_credentials):
    """Get test annotation locations."""
    location_1 = S3DataLocation(
        uri="s3://test_dir/test_annotation.json",
        location_id="MEDIA_DIR",
        credentials=s3_credentials,
    )
    location_2 = S3DataLocation(
        uri="s3://test_dir_2/test_annotation_2.json",
        location_id="MEDIA_DIR",
        credentials=s3_credentials,
    )
    return [location_1, location_2]


@pytest.fixture(name="mlflow_credentials")
def get_mlflow_credentials():
    """Get test mlflow credentials."""
    return MlflowCredentials(
        server_uri="http://test_mlflow_uri",
        s3_endpoint_url="http://test_s3_uri",
        s3_region_name="eu-central-1",
        s3_artifact_access_id="test_access_id",
        s3_artifact_access_key="test_access_key",
        run_id="test_run_id",
    )


@pytest.fixture(name="training_job")
def training_job_fixture(
    image_locations,
    checkpoint_location,
    configuration_dict,
    eval_config,
    mlflow_credentials,
    annotation_locations,
):
    """Create a prediction job."""
    return TrainingJob(
        image_locations=image_locations,
        annotation_locations=annotation_locations,
        evaluator_configuration_dict=eval_config,
        mlflow_credentials=mlflow_credentials,
        model_type_name="test_model",
        model_configuration_dict=configuration_dict,
        model_checkpoint_location=checkpoint_location,
    )


def test_training_job_creation(
    training_job,
    image_locations,
    checkpoint_location,
    configuration_dict,
    annotation_locations,
    mlflow_credentials,
    eval_config,
):
    """Test the creation of a training job."""
    job = training_job

    assert job.image_locations == image_locations
    assert job.annotation_locations == annotation_locations
    assert job.mlflow_credentials == mlflow_credentials
    assert job.evaluator_configuration_dict == eval_config
    assert job.model_type_name == "test_model"
    assert job.model_configuration_dict == configuration_dict
    assert job.model_checkpoint_location == checkpoint_location
    assert job.id is not None
    assert job.status == JobStatus.NEW
    assert job.gpu is None
    assert job.container_id is None


def test_get_environment_variables(
    training_job,
    image_locations,
    checkpoint_location,
    configuration_dict,
    eval_config,
    annotation_locations,
    mlflow_credentials,
):
    """Test if the environment variables are correctly returned."""
    environment_variables = training_job.get_environment_variables()

    assert json.loads(environment_variables["IMAGE_LOCATIONS"]) == [
        location.to_dict() for location in image_locations
    ]
    assert json.loads(environment_variables["ANNOTATION_LOCATIONS"]) == [
        location.to_dict() for location in annotation_locations
    ]
    assert environment_variables["MODEL_TYPE_NAME"] == "test_model"
    assert (
        json.loads(environment_variables["MODEL_CONFIGURATION_DICT"])
        == configuration_dict
    )
    assert json.loads(environment_variables["EVAL_CONFIGURATION_DICT"]) == eval_config
    assert (
        json.loads(environment_variables["MODEL_CHECKPOINT_LOCATION"])
        == checkpoint_location.to_dict()
    )
    assert environment_variables["JOB_TYPE"] == "TRAINING"
    assert (
        json.loads(environment_variables["MLFLOW_CREDENTIALS"])
        == mlflow_credentials.to_dict()
    )

    assert environment_variables == {
        "IMAGE_LOCATIONS": '[{"uri": "s3://test_dir/test_image.jpg", "location_id": "MEDIA_DIR", '
        '"credentials":'
        ' {"s3_endpoint_url": "http://test-s3:9000", "s3_region_name": '
        '"eu-central-1",'
        ' "s3_artifact_access_key": "123456", "s3_artifact_access_id": '
        '"testuser"}},'
        ' {"uri": "s3://test_dir_2/test_image_2.jpg", "location_id": '
        '"MEDIA_DIR", "credentials":'
        ' {"s3_endpoint_url": "http://test-s3:9000", "s3_region_name": '
        '"eu-central-1",'
        ' "s3_artifact_access_key": "123456", "s3_artifact_access_id": '
        '"testuser"}}]',
        "ANNOTATION_LOCATIONS": '[{"uri": "s3://test_dir/test_annotation.json", "location_id": '
        '"MEDIA_DIR",'
        ' "credentials": {"s3_endpoint_url": "http://test-s3:9000", '
        '"s3_region_name":'
        ' "eu-central-1", "s3_artifact_access_key": "123456", '
        '"s3_artifact_access_id":'
        ' "testuser"}}, {"uri": '
        '"s3://test_dir_2/test_annotation_2.json", "location_id":'
        ' "MEDIA_DIR", "credentials": {"s3_endpoint_url": '
        '"http://test-s3:9000",'
        ' "s3_region_name": "eu-central-1", "s3_artifact_access_key": '
        '"123456",'
        ' "s3_artifact_access_id": "testuser"}}]',
        "MODEL_TYPE_NAME": "test_model",
        "MODEL_CONFIGURATION_DICT": '{"test_key": "test_value", "test_dict": {"test_nested_key": '
        '"test_nested_value"}}',
        "MODEL_CHECKPOINT_LOCATION": '{"uri": "s3://test_dir/test_checkpoint.pth", '
        '"location_id": "MEDIA_DIR",'
        ' "credentials": {"s3_endpoint_url": "http://test-s3:9000", '
        '"s3_region_name":'
        ' "eu-central-1", "s3_artifact_access_key": "123456", '
        '"s3_artifact_access_id":'
        ' "testuser"}}',
        "EVAL_CONFIGURATION_DICT": '{"test_eval_key": "test_value", "test_eval_dict": {'
        '"test_nested_key":'
        ' "test_nested_value"}}',
        "JOB_TYPE": "TRAINING",
        "MLFLOW_CREDENTIALS": '{"server_uri": "http://test_mlflow_uri", "s3_endpoint_url": '
        '"http://test_s3_uri",'
        ' "s3_region_name": "eu-central-1", "s3_artifact_access_key": '
        '"test_access_key",'
        ' "s3_artifact_access_id": "test_access_id", "run_id": '
        '"test_run_id"}',
    }
