# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the training routes."""
import os

import pytest
from mlcvzoo_base.configuration.annotation_handler_config import (
    AnnotationHandlerConfig,
    AnnotationHandlerPASCALVOCInputDataConfig,
)
from mlcvzoo_util.adapters.adapter_registry import ImplementedMetricAdapters

from mlcvzoo_service.constants import RequestKeys
from mlcvzoo_service.tests.unit_tests.test_utils.utils import TestConfig


@pytest.fixture(name="default_params")
def generate_default_params_training(s3_credentials):
    """Generate default parameters for a TrainingJob."""
    return {
        RequestKeys.IMAGE_LOCATIONS.value: [
            {
                "adapter_type": "S3",
                "data": {
                    "uri": "https://127.0.0.1:9000/test-bucket/images",
                    "location_id": "TEST_OUTPUT",
                    "credentials": s3_credentials.to_dict(),
                },
            }
        ],
        RequestKeys.ANNOTATION_LOCATIONS.value: [
            {
                "adapter_type": "S3",
                "data": {
                    "uri": "https://127.0.0.1:9000/test-bucket/annotations",
                    "location_id": "TEST_OUTPUT",
                    "credentials": s3_credentials.to_dict(),
                },
            }
        ],
        RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value: {
            "adapter_type": "S3",
            "data": {
                "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
                "location_id": "TEST_OUTPUT",
                "credentials": s3_credentials.to_dict(),
            },
        },
        RequestKeys.RESULT_MODEL_CHECKPOINT_LOCATION.value: {
            "adapter_type": "S3",
            "data": {
                "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/result_model.pth",
                "location_id": "TEST_OUTPUT",
                "credentials": s3_credentials.to_dict(),
            },
        },
        RequestKeys.MODEL_TYPE_NAME.value: "test_model",
        RequestKeys.MODEL_CONFIG_DATA.value: TestConfig(
            unique_name="test-model"
        ).to_dict(),
        RequestKeys.EVAL_CONFIG.value: {
            "iou_thresholds": [0.5],
            "annotation_handler_config": AnnotationHandlerConfig(
                pascal_voc_input_data=[
                    AnnotationHandlerPASCALVOCInputDataConfig(
                        input_image_dir=os.path.join("/test_dir/test_data", "images"),
                        input_xml_dir=os.path.join(
                            "/test_dir/test_data",
                            "annotations/pascal_voc/dummy_task",
                        ),
                        image_format=".jpg",
                        input_sub_dirs=[],
                    )
                ],
            ).to_dict(),
        },
        RequestKeys.MLFLOW_CREDENTIALS.value: {
            "adapter_type": ImplementedMetricAdapters.MLFLOW.value,
            "data": {
                "s3_artifact_access_key": "test-key",
                "s3_artifact_access_id": "test-id",
                "s3_region_name": "eu-region-1",
                "s3_endpoint_url": "https://127.0.0.1:9000/test-bucket/",
                "server_uri": "https://127.0.0.1:5050",
                "run_id": "12345678",
            },
        },
    }


@pytest.mark.parametrize("add_job_mock", ["12345678"], indirect=True)
def test_training_route_success(create_app, default_params, add_job_mock):
    """Test if correct status code and json are returned if the training job is successfully
    created."""

    with create_app.app_context():
        response = create_app.test_client().post(
            "/training/",
            json=default_params,
        )

        assert response.status_code == 201
        assert response.json == {"job_id": "12345678"}


def test_training_route_missing_param(create_app, default_params):
    """Test if correct status code is returned if the request data is incomplete."""

    with create_app.app_context():
        default_params.pop(RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value)
        response = create_app.test_client().post(
            "/training/",
            json=default_params,
        )

        assert response.status_code == 400


def test_training_route_failed_job(create_app, default_params, add_job_fail_mock):
    """Test if correct status code is returned if an error is raised.

    It does not matter if the JobManager raises the error or if the TrainingJob raises the error.
    Both ways, a 500 status code should be returned.
    """
    with create_app.app_context():
        with pytest.raises(RuntimeError):
            response = create_app.test_client().post(
                "/training/",
                json=default_params,
            )

            assert response.status_code == 500
