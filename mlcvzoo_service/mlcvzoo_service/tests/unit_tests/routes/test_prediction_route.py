# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the prediction routes."""

import pytest

from mlcvzoo_service.constants import RequestKeys
from mlcvzoo_service.tests.unit_tests.test_utils.utils import TestConfig


@pytest.fixture(name="default_params")
def generate_default_params_predict(s3_credentials):
    """Generate default parameters for a PredictionJob."""
    return {
        RequestKeys.IMAGE_LOCATIONS.value: [
            {
                "adapter_type": "S3",
                "data": {
                    "uri": "https://127.0.0.1:9000/test-bucket/images",
                    "location_id": "TEST_OUTPUT",
                    "credentials": s3_credentials.to_dict(),
                },
            }
        ],
        RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value: {
            "adapter_type": "S3",
            "data": {
                "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints"
                "/model.pth",
                "location_id": "TEST_OUTPUT",
                "credentials": s3_credentials.to_dict(),
            },
        },
        RequestKeys.MODEL_TYPE_NAME.value: "test_model",
        RequestKeys.MODEL_CONFIG_DATA.value: TestConfig(
            unique_name="test-model"
        ).to_dict(),
    }


@pytest.fixture(name="default_params_second_model")
def generate_default_params_predict_with_second_model(s3_credentials):
    """Generate default parameters for a PredictionJob."""
    return {
        RequestKeys.IMAGE_LOCATIONS.value: [
            {
                "adapter_type": "S3",
                "data": {
                    "uri": "https://127.0.0.1:9000/test-bucket/images",
                    "location_id": "TEST_OUTPUT",
                    "credentials": s3_credentials.to_dict(),
                },
            }
        ],
        RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value: {
            "adapter_type": "S3",
            "data": {
                "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints"
                "/model.pth",
                "location_id": "TEST_OUTPUT",
                "credentials": s3_credentials.to_dict(),
            },
        },
        RequestKeys.MODEL_TYPE_NAME.value: "test_model",
        RequestKeys.MODEL_CONFIG_DATA.value: TestConfig(
            unique_name="test-model"
        ).to_dict(),
        RequestKeys.SECOND_MODEL_TYPE_NAME.value: "test_model_2",
        RequestKeys.SECOND_MODEL_CONFIG_DATA.value: TestConfig(
            unique_name="test-model-2"
        ).to_dict(),
        RequestKeys.SECOND_MODEL_CHECKPOINT_LOCATION.value: {
            "adapter_type": "S3",
            "data": {
                "uri": "https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints"
                "/model.pth",
                "location_id": "TEST_OUTPUT",
                "credentials": s3_credentials.to_dict(),
            },
        },
    }


@pytest.mark.parametrize("add_job_mock", ["12345678"], indirect=True)
def test_prediction_route_success(create_app, default_params, add_job_mock):
    """Test if correct status code and json are returned if the prediction job is successfully
    created."""

    with create_app.app_context():
        response = create_app.test_client().post(
            "/prediction/",
            json=default_params,
        )

        assert response.status_code == 201
        assert response.json == {"job_id": "12345678"}


@pytest.mark.parametrize("add_job_mock", ["12345678"], indirect=True)
def test_prediction_route_success_second_model(
    create_app, default_params_second_model, add_job_mock
):
    """Test if correct status code and json are returned if the prediction job is successfully
    created with second model params."""

    with create_app.app_context():
        response = create_app.test_client().post(
            "/prediction/",
            json=default_params_second_model,
        )

        assert response.status_code == 201
        assert response.json == {"job_id": "12345678"}


def test_prediction_route_missing_param(create_app, default_params):
    """Test if correct status code is returned if the request data is incomplete."""

    with create_app.app_context():
        default_params.pop(RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value)
        response = create_app.test_client().post(
            "/prediction/",
            json=default_params,
        )

        assert response.status_code == 400


def test_prediction_route_failed_job(create_app, default_params, add_job_fail_mock):
    """Test if correct status code is returned if an error is raised.

    It does not matter if the JobManager raises the error or if the TrainingJob raises the error.
    Both ways, a 500 status code should be returned.
    """

    with create_app.app_context():
        with pytest.raises(RuntimeError):
            response = create_app.test_client().post(
                "/prediction/",
                json=default_params,
            )

            assert response.status_code == 500
