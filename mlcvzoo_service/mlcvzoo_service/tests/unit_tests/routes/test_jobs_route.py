# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the jobs routes."""

from unittest.mock import MagicMock, PropertyMock

import pytest

from mlcvzoo_service.services.jobs.constants import JobStatus


@pytest.fixture(name="read_job_logs_no_logs")
def job_logs_no_logs_fixture(mocker):
    """Get job logs with empty logs."""
    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.read_job_logs",
        return_value="",
    )


@pytest.fixture(name="read_job_logs")
def job_logs_fixture(mocker):
    """Get job logs."""
    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.read_job_logs",
        return_value="This are the logs",
    )


@pytest.fixture(name="get_job_status")
def job_status():
    """Get job status."""
    return (
        JobStatus.FINISHED,
        {"test_dir/test_image.jpg": {"xmin": 0, "ymin": 0, "xmax": 1, "ymax": 1}},
    )


@pytest.fixture(name="all_jobs")
def all_jobs_fixture(mocker, get_all_jobs):
    """Mock the JobManager.all_jobs property."""
    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.all_jobs",
        return_value=get_all_jobs,
        new_callable=PropertyMock,
    )


@pytest.fixture(name="running_jobs")
def running_jobs_fixture(mocker, get_running_jobs):
    """Mock the JobManager.running_jobs property."""
    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.running_jobs",
        return_value=get_running_jobs,
        new_callable=PropertyMock,
    )


@pytest.fixture(name="finished_jobs")
def finished_jobs_fixture(mocker, get_finished_jobs):
    """Mock the JobManager.running_jobs property."""
    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.finished_jobs",
        return_value=get_finished_jobs,
        new_callable=PropertyMock,
    )


@pytest.fixture(name="queued_jobs")
def queued_jobs_fixture(mocker, get_queued_jobs):
    """Mock the JobManager.running_jobs property."""
    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.queued_jobs",
        return_value=get_queued_jobs,
        new_callable=PropertyMock,
    )


@pytest.fixture(name="crashed_jobs")
def crashed_jobs_fixture(mocker, get_crashed_jobs):
    """Mock the JobManager.running_jobs property."""
    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.crashed_jobs",
        return_value=get_crashed_jobs,
        new_callable=PropertyMock,
    )


@pytest.fixture(name="job_status")
def job_status_fixture(mocker, get_job_status, request):
    """Mock the JobManager.get_job_status method.

    If no parameter is passed, the job status is returned. Otherwise, None is returned.
    """
    if request.param == "success":
        mocker.patch(
            "mlcvzoo_service.services.job_manager.job_manager._JobManager.get_job_status",
            return_value=get_job_status,
        )
    else:
        mocker.patch(
            "mlcvzoo_service.services.job_manager.job_manager._JobManager.get_job_status",
            return_value=(None, None),
        )


@pytest.fixture(name="finish_job")
def finish_job_fixture(mocker):
    """Mock the JobManager.finish_job method."""
    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.finish_job"
    )


@pytest.fixture(name="get_job")
def get_job_fixture(mocker, request):
    """Mock the JobManager.get_job method."""
    job = MagicMock()
    if request.param == "finished":
        job.status.value = "FINISHED"
    elif request.param == "crashed":
        job.status.value = "CRASHED"
    elif request.param == "not_found":
        job = None

    mocker.patch(
        "mlcvzoo_service.services.job_manager.job_manager._JobManager.get_job",
        return_value=job,
    )


def test_get_all_jobs(create_app, all_jobs):
    """Test if correct status code and data is returned if all jobs are got successfully."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/")
        assert response.status_code == 200
        assert response.get_json() == {
            "jobs": [
                {
                    "job": "1",
                    "status": "RUNNING",
                    "gpu": 0,
                    "container": "123",
                    "results": {},
                },
                {
                    "job": "2",
                    "status": "CRASHED",
                    "gpu": None,
                    "container": "456",
                    "results": {},
                },
                {
                    "job": "3",
                    "status": "FINISHED",
                    "gpu": None,
                    "container": "789",
                    "results": {},
                },
            ]
        }


def test_get_running_jobs(create_app, running_jobs):
    """Test if correct status code and data is returned if all running jobs are got
    successfully."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/running")
        assert response.status_code == 200
        assert response.get_json() == {
            "jobs": [
                {
                    "job": "4",
                    "status": "RUNNING",
                    "gpu": 0,
                    "container": "456",
                    "results": {},
                },
                {
                    "job": "5",
                    "status": "RUNNING",
                    "gpu": 1,
                    "container": "567",
                    "results": {},
                },
            ]
        }


def test_get_finished_jobs(create_app, finished_jobs):
    """Test if correct status code and data is returned if all finished jobs are got
    successfully."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/finished")
        assert response.status_code == 200
        assert response.get_json() == {
            "jobs": [
                {
                    "job": "6",
                    "status": "FINISHED",
                    "gpu": None,
                    "container": "678",
                    "results": {},
                },
                {
                    "job": "7",
                    "status": "FINISHED",
                    "gpu": None,
                    "container": "789",
                    "results": {},
                },
            ]
        }


def test_get_queued_jobs(create_app, queued_jobs):
    """Test if correct status code and data is returned if all queued jobs are got successfully."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/queued")
        assert response.status_code == 200
        assert response.get_json() == {
            "jobs": [
                {
                    "job": "8",
                    "status": "NEW",
                    "gpu": None,
                    "container": "891",
                    "results": {},
                },
                {
                    "job": "9",
                    "status": "NEW",
                    "gpu": None,
                    "container": "912",
                    "results": {},
                },
            ]
        }


def test_get_crashed_jobs(create_app, crashed_jobs):
    """Test if correct status code and data is returned if all crashed jobs are got
    successfully."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/crashed")
        assert response.status_code == 200
        assert response.get_json() == {
            "jobs": [
                {
                    "job": "10",
                    "status": "CRASHED",
                    "gpu": None,
                    "container": "101",
                    "results": {},
                },
                {
                    "job": "11",
                    "status": "CRASHED",
                    "gpu": None,
                    "container": "111",
                    "results": {},
                },
            ]
        }


@pytest.mark.parametrize("job_status", ["success"], indirect=True)
def test_get_job_status(create_app, job_status):
    """Test if correct status code and data is returned if the job status is got successfully."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/12345678")
        assert response.status_code == 200
        assert response.get_json() == {
            "job_status": "FINISHED",
            "job_results": {
                "test_dir/test_image.jpg": {"xmin": 0, "ymin": 0, "xmax": 1, "ymax": 1}
            },
        }


@pytest.mark.parametrize("job_status", ["fail"], indirect=True)
def test_get_job_status_fail(create_app, job_status):
    """Test if correct status code is returned if the job status is not found."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/12345678")
        assert response.status_code == 204


@pytest.mark.parametrize("get_job", ["finished"], indirect=True)
def test_finish_job_already_finished(create_app, get_job):
    """Test if correct status code is returned if the job is already finished."""
    with create_app.app_context():
        response = create_app.test_client().put("/jobs/12345678/finish", json={})
        assert response.status_code == 409


@pytest.mark.parametrize("get_job", ["crashed"], indirect=True)
def test_finish_job_already_crashed(create_app, get_job):
    """Test if correct status code is returned if the job is already crashed."""
    with create_app.app_context():
        response = create_app.test_client().put("/jobs/12345678/finish", json={})
        assert response.status_code == 409


@pytest.mark.parametrize("get_job", ["not_found"], indirect=True)
def test_finish_job_not_found(create_app, get_job):
    """Test if correct status code is returned if the job is not found."""
    with create_app.app_context():
        response = create_app.test_client().put("/jobs/12345678/finish", json={})
        assert response.status_code == 404


@pytest.mark.parametrize("get_job", ["succeed"], indirect=True)
def test_finish_job_succeed(create_app, get_job, finish_job):
    """Test if correct status code is returned if the job is successfully finished."""
    with create_app.app_context():
        response = create_app.test_client().put("/jobs/12345678/finish", json={})
        assert response.status_code == 200


def test_get_job_log(create_app, read_job_logs):
    """Test if correct status code and data is returned if the job logs are got successfully."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/12345678/logs")
        assert response.status_code == 200
        assert response.get_json() == {"logs": "This are the logs"}


def test_get_job_log_no_log(create_app, read_job_logs_no_logs):
    """Test if correct status code and data is returned if there are no job logs."""
    with create_app.app_context():
        response = create_app.test_client().get("/jobs/12345678/logs")
        assert response.status_code == 204
