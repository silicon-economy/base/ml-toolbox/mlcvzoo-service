# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""API specification.

All resources are added in their own namespaces.
"""

from flask import Blueprint
from flask_restx import Api

from mlcvzoo_service.api.models.flask_restx_models import api as restx_models
from mlcvzoo_service.api.routes.jobs import api as jobs_api
from mlcvzoo_service.api.routes.prediction import api as prediction_api
from mlcvzoo_service.api.routes.training import api as training_api

# create the api endpoint
api_bp = Blueprint("api", __name__)
api = Api(api_bp, version="2.0", title="MLCVZoo-Service API")

# add namespace here
api.add_namespace(jobs_api)
api.add_namespace(training_api)
api.add_namespace(prediction_api)
api.add_namespace(restx_models)
