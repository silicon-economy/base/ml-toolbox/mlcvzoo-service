# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""HTTP REST API for training management."""

import logging
from typing import Any, Dict, List, Tuple, Union

from config_builder.json_encoding import TupleEncoder
from flask_restx import Namespace, Resource
from mlcvzoo_util.adapters.adapter_registry import adapter_registry
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service.api.models.flask_restx_models import (
    JobResponseModels,
    PredictionRESTXModels,
)
from mlcvzoo_service.constants import RequestKeys, RequestParams
from mlcvzoo_service.services.job_manager.job_manager import global_job_manager
from mlcvzoo_service.services.jobs.prediction.prediction_job import PredictionJob

api = Namespace("prediction", description="Everything related to prediction jobs.")

logger = logging.getLogger(__file__)


@api.route("/")
class Prediction(Resource):
    """Resource for prediction jobs."""

    @api.doc("create_new_prediction_job")
    @api.marshal_with(
        fields=JobResponseModels.job_id_response_model,
        code=201,
        description="Successfully created and started a new prediction job.",
    )
    @api.response(code=400, description="Error while validating your request data.")
    @api.response(code=500, description="Error while starting prediction job.")
    @api.expect(PredictionRESTXModels.prediction_job_model, validate=True)
    def post(self) -> Tuple[Dict[str, str], int]:
        """Post a new prediction job."""

        params = TupleEncoder.decode(api.payload)

        image_locations: List[Union[S3DataLocation]] = []
        for location in params[RequestKeys.IMAGE_LOCATIONS.value]:
            image_locations.append(
                adapter_registry[location[RequestParams.ADAPTER_TYPE.value]](
                    **location[RequestParams.DATA.value]
                )
            )

        model_checkpoint_location: Union[S3DataLocation] = adapter_registry[
            params[RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value][
                RequestParams.ADAPTER_TYPE.value
            ]
        ](
            **params[RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value][
                RequestParams.DATA.value
            ]
        )

        model_type_name: str = params[RequestKeys.MODEL_TYPE_NAME.value]
        model_configuration_dict: Dict[str, Any] = params[
            RequestKeys.MODEL_CONFIG_DATA.value
        ]

        if (
            params.get(RequestKeys.SECOND_MODEL_CHECKPOINT_LOCATION.value, None)
            is not None
        ):
            second_model_checkpoint_location: Union[S3DataLocation] = adapter_registry[
                params[RequestKeys.SECOND_MODEL_CHECKPOINT_LOCATION.value][
                    RequestParams.ADAPTER_TYPE.value
                ]
            ](
                **params[RequestKeys.SECOND_MODEL_CHECKPOINT_LOCATION.value][
                    RequestParams.DATA.value
                ]
            )
            second_model_type_name: str = params[
                RequestKeys.SECOND_MODEL_TYPE_NAME.value
            ]
            second_model_configuration_dict: Dict[str, Any] = params[
                RequestKeys.SECOND_MODEL_CONFIG_DATA.value
            ]
            new_job = PredictionJob(
                image_locations=image_locations,
                model_checkpoint_location=model_checkpoint_location,
                model_type_name=model_type_name,
                model_configuration_dict=model_configuration_dict,
                second_model_checkpoint_location=second_model_checkpoint_location,
                second_model_type_name=second_model_type_name,
                second_model_configuration_dict=second_model_configuration_dict,
            )

        else:
            new_job = PredictionJob(
                image_locations=image_locations,
                model_checkpoint_location=model_checkpoint_location,
                model_type_name=model_type_name,
                model_configuration_dict=model_configuration_dict,
            )

        job_id = global_job_manager.add_job(job=new_job)

        return {"job_id": str(job_id)}, 201
