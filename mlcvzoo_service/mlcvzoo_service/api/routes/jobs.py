# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""HTTP REST API for job management."""
import logging
from typing import Any, Dict, Tuple

from flask_restx import Namespace, Resource

from mlcvzoo_service.api.models.flask_restx_models import JobResponseModels
from mlcvzoo_service.services.job_manager.job_manager import global_job_manager
from mlcvzoo_service.services.jobs.constants import JobStatus

api = Namespace("jobs", description="Everything related to jobs.")

logger = logging.getLogger(__file__)


@api.route("/")
class JobListAll(Resource):
    """Resource for multiple (all) jobs."""

    @api.doc("list_jobs")
    @api.marshal_with(
        fields=JobResponseModels.job_list_response_model,
        code=200,
        description="Successfully retrieved list of all jobs and their current "
        "status.",
    )
    @api.response(code=500, description="Error while retrieving jobs.")
    def get(self) -> Any:
        """Get all jobs and their status."""
        jobs = {
            "jobs": [
                {
                    "job": str(job.id),
                    "status": job.status.value,
                    "gpu": job.gpu,
                    "container": job.container_id,
                    "results": job.results,
                }
                for job in global_job_manager.all_jobs
            ]
        }
        return jobs, 200


@api.route("/running")
class JobListRunning(Resource):
    """Resource for running jobs."""

    @api.doc("list_jobs")
    @api.marshal_with(
        fields=JobResponseModels.job_list_response_model,
        code=200,
        description="Successfully retrieved list of all running jobs and their current "
        "status.",
    )
    @api.response(code=500, description="Error while retrieving jobs.")
    def get(self) -> Any:
        """Get all running jobs and their status."""
        jobs = {
            "jobs": [
                {
                    "job": str(job.id),
                    "status": job.status.value,
                    "gpu": job.gpu,
                    "container": job.container_id,
                    "results": job.results,
                }
                for job in global_job_manager.running_jobs
            ]
        }
        return jobs, 200


@api.route("/finished")
class JobListFinished(Resource):
    """Resource for finished jobs."""

    @api.doc("list_jobs")
    @api.marshal_with(
        fields=JobResponseModels.job_list_response_model,
        code=200,
        description="Successfully retrieved list of finished jobs and their current "
        "status.",
    )
    @api.response(code=500, description="Error while retrieving jobs.")
    def get(self) -> Any:
        """Get all finished jobs and their status."""
        jobs = {
            "jobs": [
                {
                    "job": str(job.id),
                    "status": job.status.value,
                    "gpu": job.gpu,
                    "container": job.container_id,
                    "results": job.results,
                }
                for job in global_job_manager.finished_jobs
            ]
        }
        return jobs, 200


@api.route("/queued")
class JobListQueued(Resource):
    """Resource for queued jobs."""

    @api.doc("list_jobs")
    @api.marshal_with(
        fields=JobResponseModels.job_list_response_model,
        code=200,
        description="Successfully retrieved list of queued jobs and their current "
        "status.",
    )
    @api.response(code=500, description="Error while retrieving jobs.")
    def get(self) -> Any:
        """Get all queued jobs and their status."""
        jobs = {
            "jobs": [
                {
                    "job": str(job.id),
                    "status": job.status.value,
                    "gpu": job.gpu,
                    "container": job.container_id,
                    "results": job.results,
                }
                for job in global_job_manager.queued_jobs
            ]
        }
        return jobs, 200


@api.route("/crashed")
class JobListCrashed(Resource):
    """Resource for queued jobs."""

    @api.doc("list_jobs")
    @api.marshal_with(
        fields=JobResponseModels.job_list_response_model,
        code=200,
        description="Successfully retrieved list of crashed jobs and their current "
        "status.",
    )
    @api.response(code=500, description="Error while retrieving jobs.")
    def get(self) -> Any:
        """Get all queued jobs and their status."""
        jobs = {
            "jobs": [
                {
                    "job": str(job.id),
                    "status": job.status.value,
                    "gpu": job.gpu,
                    "container": job.container_id,
                    "results": job.results,
                }
                for job in global_job_manager.crashed_jobs
            ]
        }
        return jobs, 200


@api.route("/<string:job_id>")
class Job(Resource):
    """Resource for a single job."""

    @api.doc("get_single_job")
    @api.marshal_with(
        fields=JobResponseModels.single_job_response_model,
        code=200,
        description="Successfully retrieved status and results of desired job.",
    )
    @api.response(code=204, description="The requested job ID was not found.")
    @api.response(code=500, description="Error while retrieving job.")
    def get(self, job_id: str) -> Tuple[Dict[str, Any], int]:
        """Get the status and results of a single job."""
        _job_status, _job_results = global_job_manager.get_job_status(job_id=job_id)

        if _job_status is None:
            return {"message": "Job not found"}, 204
        else:
            return (
                {
                    "job_status": str(_job_status.value),
                    "job_results": _job_results,
                },
                200,
            )


@api.route("/<string:job_id>/finish")
class JobFinish(Resource):
    """Resource to finish a single job."""

    @api.doc("finish_job")
    @api.response(
        code=200, description="Successfully finished the job with given job ID."
    )
    @api.response(code=404, description="The requested job ID was not found.")
    @api.response(code=409, description="The job is already finished.")
    @api.response(code=500, description="Error while finishing job.")
    def put(self, job_id: str) -> Tuple[Dict[str, Any], int]:
        """Finish a job."""
        job = global_job_manager.get_job(job_id=job_id)
        params = api.payload

        if job is None:
            return {"message": "Job not found"}, 404

        elif job.status.value == JobStatus.FINISHED.value:
            return {"message": "Job already finished"}, 409

        elif job.status.value == JobStatus.CRASHED.value:
            return {"message": "Job already crashed"}, 409

        else:
            global_job_manager.finish_job(job_id=job_id, results=params)
            return {"message": "Job finished"}, 200


@api.route("/<string:job_id>/logs")
class JobLogs(Resource):
    """Resource to get logs of a single job."""

    @api.doc("get_logs")
    @api.marshal_with(
        fields=JobResponseModels.job_logs_response_model,
        code=200,
        description="Successfully retrieved logs of desired job.",
    )
    @api.response(
        code=204,
        description="The logs for the requested job ID were not found or are empty.",
    )
    @api.response(code=500, description="Error while retrieving logs.")
    def get(self, job_id: str) -> Tuple[Dict[str, Any], int]:
        """Get the logs of a single job."""
        job_logs = global_job_manager.read_job_logs(job_id=job_id)

        if job_logs == "":
            return {"message": "Logs for job not found or empty"}, 204

        return {"logs": job_logs}, 200
