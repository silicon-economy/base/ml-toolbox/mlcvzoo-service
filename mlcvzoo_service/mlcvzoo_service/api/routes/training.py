# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""HTTP REST API for training management."""
import logging
from typing import Any, Dict, List, Tuple, Union

from config_builder.json_encoding import TupleEncoder
from flask_restx import Namespace, Resource
from mlcvzoo_util.adapters.adapter_registry import adapter_registry
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service.api.models.flask_restx_models import (
    JobResponseModels,
    TrainingRESTXModels,
)
from mlcvzoo_service.constants import RequestKeys, RequestParams
from mlcvzoo_service.services.job_manager.job_manager import global_job_manager
from mlcvzoo_service.services.jobs.training.training_job import TrainingJob

api = Namespace("training", description="Everything related to training jobs.")

logger = logging.getLogger(__file__)


@api.route("/")
class Training(Resource):
    """Resource for training jobs."""

    @api.doc("create_new_training_job")
    @api.marshal_with(
        fields=JobResponseModels.job_id_response_model,
        code=201,
        description="Successfully created and started a new training job.",
    )
    @api.response(code=400, description="Error while validating your request data.")
    @api.response(code=500, description="Error while starting training job.")
    @api.expect(TrainingRESTXModels.training_job_model, validate=True)
    def post(self) -> Tuple[Dict[str, str], int]:
        """Post a new training job."""

        params = TupleEncoder.decode(api.payload)

        image_locations: List[Union[S3DataLocation]] = []
        for location in params[RequestKeys.IMAGE_LOCATIONS.value]:
            image_locations.append(
                adapter_registry[location[RequestParams.ADAPTER_TYPE.value]](
                    **location[RequestParams.DATA.value]
                )
            )

        annotation_locations: List[Union[S3DataLocation]] = []
        for location in params[RequestKeys.ANNOTATION_LOCATIONS.value]:
            annotation_locations.append(
                adapter_registry[location[RequestParams.ADAPTER_TYPE.value]](
                    **location[RequestParams.DATA.value]
                )
            )

        model_checkpoint_location: Union[S3DataLocation] = adapter_registry[
            params[RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value][
                RequestParams.ADAPTER_TYPE.value
            ]
        ](
            **params[RequestKeys.INITIAL_MODEL_CHECKPOINT_LOCATION.value][
                RequestParams.DATA.value
            ]
        )

        model_type_name: str = params[RequestKeys.MODEL_TYPE_NAME.value]
        model_configuration_dict: Dict[str, Any] = params[
            RequestKeys.MODEL_CONFIG_DATA.value
        ]
        evaluator_configuration_dict: Dict[str, Any] = params[
            RequestKeys.EVAL_CONFIG.value
        ]

        mlflow_credentials: MlflowCredentials = adapter_registry[
            params[RequestKeys.MLFLOW_CREDENTIALS.value][
                RequestParams.ADAPTER_TYPE.value
            ]
        ](**params[RequestKeys.MLFLOW_CREDENTIALS.value][RequestParams.DATA.value])

        new_job = TrainingJob(
            image_locations=image_locations,
            annotation_locations=annotation_locations,
            model_checkpoint_location=model_checkpoint_location,
            model_type_name=model_type_name,
            model_configuration_dict=model_configuration_dict,
            evaluator_configuration_dict=evaluator_configuration_dict,
            mlflow_credentials=mlflow_credentials,
        )

        job_id = global_job_manager.add_job(job=new_job)

        return {"job_id": str(job_id)}, 201
