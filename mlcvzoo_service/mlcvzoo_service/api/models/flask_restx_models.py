# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining flask_restx models."""

from flask_restx import Namespace, fields

api = Namespace("Flask Restx Models", description="The flask restx models")


# TODO: Move the shared RESTX models to mlcvzoo-util?
class CommonRESTXModels:
    """Common RESTX models utilized by other models."""

    s3_credentials_attributes_model = api.model(
        name="S3CredentialsAttributes",
        model={
            "s3_artifact_access_key": fields.String(
                required=True, description="The key to access the S3 storage."
            ),
            "s3_artifact_access_id": fields.String(
                required=True, description="The ID to access the S3 storage."
            ),
            "s3_region_name": fields.String(
                required=True, description="The region of the S3 storage."
            ),
            "s3_endpoint_url": fields.String(
                required=True,
                description="The unique resource identifier inside the S3 storage of the model.",
            ),
        },
        description="All credentials necessary to access the desired S3 storage.",
    )

    s3_data_location_attributes_model = api.model(
        name="S3DataLocationAttributes",
        model={
            "uri": fields.String(required=True, description="The URI of the file."),
            "location_id": fields.String(
                required=True, description="The location ID of the file."
            ),
            "credentials": fields.Nested(
                model=s3_credentials_attributes_model, required=False
            ),
        },
        description="The location of a file and the credentials to access it.",
    )

    s3_data_location_model = api.model(
        name="S3DataLocation",
        model={
            "data": fields.Nested(model=s3_data_location_attributes_model),
            "adapter_type": fields.String(
                required=True,
                description="The adapter of this model.",
            ),
        },
        description="The location of a file and the necessary adapter type to access it.",
    )

    mlflow_credentials_attributes_model = api.model(
        name="MlflowCredentialsAttributes",
        model={
            "s3_artifact_access_key": fields.String(
                required=True, description="The key to access the S3 storage."
            ),
            "s3_artifact_access_id": fields.String(
                required=True, description="The ID to access the S3 storage."
            ),
            "s3_region_name": fields.String(
                required=True, description="The region of the S3 storage."
            ),
            "s3_endpoint_url": fields.String(
                required=True,
                description="The unique resource identifier inside the S3 storage of the model.",
            ),
            "server_uri": fields.String(
                required=True,
                description="The unique resource identifier of the S3 server.",
            ),
            "run_id": fields.String(
                required=True, description="The Mlflow run ID of the model."
            ),
        },
        description="All credentials necessary to access the desired Mlflow server.",
    )

    mlflow_credentials_model = api.model(
        name="MlflowCredentials",
        model={
            "data": fields.Nested(model=mlflow_credentials_attributes_model),
            "adapter_type": fields.String(
                required=True,
                description="The adapter type of this model.",
            ),
        },
        description="The credentials of a registered Mlflow model and the necessary adapter "
        "type to access it.",
    )

    config_dict_model = api.model(
        name="ConfigDict",
        model={},
        description="A placeholder for mlcvzoo internal configurations of different types. "
        "As the configurations and the keys used are very different, this model only "
        "assumes that the configuration is a dictionary.",
    )


class PredictionRESTXModels:
    """RESTX models for prediction endpoints."""

    prediction_job_model = api.model(
        name="PredictionJobModel",
        model={
            "MODEL_TYPE_NAME": fields.String(
                required=True,
                description="The name of the model type to determine "
                "the correct MLCVZoo model registry entry.",
            ),
            "MODEL_CONFIG_DATA": fields.Nested(
                model=CommonRESTXModels.config_dict_model,
                description="The model config.",
                required=True,
            ),
            "INITIAL_MODEL_CHECKPOINT_LOCATION": fields.Nested(
                model=CommonRESTXModels.s3_data_location_model,
                description="The location of the model checkpoint used for prediction.",
                required=True,
            ),
            "IMAGE_LOCATIONS": fields.List(
                cls_or_instance=fields.Nested(CommonRESTXModels.s3_data_location_model),
                required=True,
                description="The location of the images to predict.",
            ),
            # Second model is for text recognition jobs only and therefore optional
            "SECOND_MODEL_TYPE_NAME": fields.String(
                required=False,
                description="The name of the second model type to determine "
                "the correct MLCVZoo model registry entry.",
            ),
            "SECOND_MODEL_CONFIG_DATA": fields.Nested(
                model=CommonRESTXModels.config_dict_model,
                description="The second model config.",
                required=False,
            ),
            "SECOND_MODEL_CHECKPOINT_LOCATION": fields.Nested(
                model=CommonRESTXModels.s3_data_location_model,
                description="The location of the second model checkpoint used for prediction.",
                required=False,
            ),
        },
    )

    prediction_job_start_response_model = api.model(
        name="PredictionJobStartResponseModel",
        model={
            "job_id": fields.String(
                required=True, description="The ID of the created prediction job."
            )
        },
    )


class TrainingRESTXModels:
    """RESTX models for training endpoints."""

    training_job_model = api.model(
        name="TrainingJobModel",
        model={
            "MODEL_TYPE_NAME": fields.String(
                required=True,
                description="The name of the model type to determine "
                "the correct MLCVZoo model registry entry.",
            ),
            "MODEL_CONFIG_DATA": fields.Nested(
                model=CommonRESTXModels.config_dict_model,
                description="The model config.",
                required=True,
            ),
            "INITIAL_MODEL_CHECKPOINT_LOCATION": fields.Nested(
                model=CommonRESTXModels.s3_data_location_model,
                description="The location of the model checkpoint used for first training epoch.",
                required=True,
            ),
            "IMAGE_LOCATIONS": fields.List(
                cls_or_instance=fields.Nested(CommonRESTXModels.s3_data_location_model),
                required=True,
                description="The location of the images used for training.",
            ),
            "ANNOTATION_LOCATIONS": fields.List(
                cls_or_instance=fields.Nested(CommonRESTXModels.s3_data_location_model),
                required=True,
                description="The location of the annotations used for training.",
            ),
            "EVAL_CONFIG": fields.Nested(
                model=CommonRESTXModels.config_dict_model,
                description="The evaluator config specifying the evaluation of the training "
                "checkpoints.",
                required=True,
            ),
            "MLFLOW_CREDENTIALS": fields.Nested(
                model=CommonRESTXModels.mlflow_credentials_model,
                description="The Mlflow credentials to access the desired Mlflow server.",
                required=True,
            ),
        },
    )


class JobResponseModels:
    """RESTX models for job responses."""

    job_id_response_model = api.model(
        name="JobIDResponseModel",
        model={
            "job_id": fields.String(
                required=True, description="The ID of the created job."
            )
        },
    )

    job_status_response_model = api.model(
        name="JobStatusResponseModel",
        model={
            "job": fields.String(required=True, description="The ID of the job."),
            "status": fields.String(
                required=True, description="The current status of the job."
            ),
            "gpu": fields.Integer(
                required=False, description="The GPU the job is running on."
            ),
            "container": fields.String(
                required=False, description="The container ID of the job."
            ),
            "results": fields.Raw(
                required=False,
                description="The results of the job in the format of the job.",
            ),
        },
    )

    job_list_response_model = api.model(
        name="JobListResponseModel",
        model={
            "jobs": fields.List(
                fields.Nested(model=job_status_response_model),
                required=True,
                description="The list of jobs and their current status.",
            )
        },
    )

    job_results_response_model = api.model(
        name="JobResultsResponseModel",
        model={
            "job_id": fields.String(required=True, description="The ID of the job."),
            "results": fields.Raw(
                required=True,
                description="The results of the job in the format of the job.",
            ),
        },
    )

    single_job_response_model = api.model(
        name="SingleJobResponseModel",
        model={
            "job_status": fields.String(
                required=True, description="The current status of the job."
            ),
            "job_results": fields.Raw(
                required=True,
                description="The results of the job in the format of the job.",
            ),
        },
    )

    job_error_response_model = api.model(
        name="JobErrorResponseModel",
        model={
            "job_id": fields.String(required=True, description="The ID of the job."),
            "error": fields.String(
                required=True, description="The error message of the job."
            ),
        },
    )

    job_logs_response_model = api.model(
        name="JobLogsResponseModel",
        model={"logs": fields.String(description="The content of the log file")},
    )
