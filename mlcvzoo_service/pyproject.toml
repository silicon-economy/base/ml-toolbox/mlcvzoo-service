[project]
name = "mlcvzoo_service"
description = "MLCVZoo Service Package"
dynamic = ["version"]
license = { text = "Open Logistics Foundation License 1.3" }
readme = "README.md"
classifiers = [
  "Development Status :: 5 - Production/Stable",
  "Intended Audience :: Developers",
  "Natural Language :: English",
]
authors = [
  { name = "Maximilian Otten, email =  <maximilian.otten@iml.fraunhofer.de>" },
  { name = "Christian Hoppe, email =  <christian.hoppe@iml.fraunhofer.de>" },
  { name = "Oliver Bredtmann, email =  <oliver.bredtmann@dbschenker.com>" },
  { name = "Thilo Bauer, email =  <thilo.bauer@dbschenker.com>" },
  { name = "Oliver Urbann, email =  <oliver.urbann@iml.fraunhofer.de>" },
  { name = "Jan Basrawi, email =  <jan.basrawi@dbschenker.com>" },
  { name = "Luise Weickhmann, email =  <luise.weickhmann@iml.fraunhofer.de>" },
  { name = "Luca Kotulla, email =  <luca.kotulla@iml.fraunhofer.de>" },
]

requires-python = ">=3.8,<3.11"

dependencies = [
  "yaml-config-builder>=8.3.0, <9.0.0",
  "related-mltoolbox>=1.1.0, <2.0.0",
  "mlcvzoo_base>=6.2.0,<7",
  "mlcvzoo-util>=1.1,<2",
  "flask>=2.0.0, <3.0.0",
  "flask-restx>=1.3.0, <2.0.0",
  # numpy 1.19.2 is oldest available on aarch64 but 1.19.5 leads
  # to unbuildable pytorch there, all is well on amd64
  "numpy>=1.19.2, !=1.19.5",
  "opencv-python>=4.5.0.0, !=4.5.5.64",
  "opencv-contrib-python>=4.5.0.0, !=4.5.5.64",
  # torch 2.0.1 has missing cuda dependencies https://github.com/pytorch/pytorch/issues/100974
  # torch 2.1.0 has error:
  # - OSError: libcublas.so.11: cannot open shared object file:
  # - https://github.com/pytorch/pytorch/issues/89417
  "torch>=2.0.0,!=2.0.1,!=2.1.0",
  "torchvision>=0.10.0, <1.0.0",
  "mlflow==2.14.1",
]

[project.optional-dependencies]
dev = [
  "mock>=4.0.0, <5.0.0",
  "pytest>=7.0.0, <8.0.0",
  "pytest-cov>=3.0.0, <4.0.0",
  "pytest-mock>=3.7.0, <4.0.0",
  "flask_testing>=0.8.0, <1.0.0",
  "black>=22.0.0, <23.0.0",
  "mypy>=0.961.0, <1.0.0",
  "pylint>=2.9.6, <3.0.0",
  # Isort guarantees formatting results for 5.X
  "isort>=5.0.0, <6.0.0",
]
# We want to use the feature 'Automatically set class-mapping config for nested datasets'
# for training RTMDet models
# Issue: AssertionError: MMDetection 3.2.0 is incompatible with MMOCR 1.0.1. Please use MMDetection >= 3.0.0rc5, < 3.2.0 instead.
# With mmdet 3.2.0 comes:  AssertionError: MMCV==2.1.0 is used but incompatible. Please install mmcv>=2.0.0rc4, <2.1.0
mmdetection = ["mlcvzoo_mmdetection>=6.8", "mmdet<3.2.0", "mmcv<2.1.0"]
# TODO: Use extras once versions are fixed [tensorrt,onnx]
yolox = ["mlcvzoo_yolox>=6.2.0, <7.0.0"]
mmrotate = ["mlcvzoo_mmrotate>=0.1,<2", "mmdet<3.2.0", "mmcv<2.1.0"]
mmpretrain = ["mlcvzoo_mmpretrain>=0.1,<2"]

mmocr = ["mlcvzoo-mmocr>=6.4"]

[project.urls]
homepage = "https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/mlcvzoo-service"
repository = "https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/mlcvzoo-service"
documentation = "https://git.openlogisticsfoundation.org/silicon-economy/base/ml-toolbox/mlcvzoo-service/-/blob/main/documentation/index.adoc"

[tool.setuptools.dynamic]
version = { attr = "mlcvzoo_service.__version__" }

[tool.setuptools]
include-package-data = false

[tool.setuptools.packages.find]
where = ["."]  # list of folders that contain the packages (["."] by default)
include = ["mlcvzoo_service*"]  # package names should match these glob patterns (["*"] by default)
exclude = ["mlcvzoo_service.tests*"]  # exclude packages matching these glob patterns (empty by default)
namespaces = false  # to disable scanning PEP 420 namespaces (true by default)

[build-system]
# NOTE: Don't remove setuptools, therefore require it from the build system
requires = [
  "setuptools>=42,<70",
  "wheel",
  "setuptools_scm[toml]>=3.4"
]
build-backend = "setuptools.build_meta"

[tool.isort]
profile = "black"

[tool.pylint.master]
extension-pkg-whitelist = ["numpy", "cv2"]
jobs = 0

[tool.pytest.ini_options]
log_cli = 1
log_cli_level = "DEBUG"
log_cli_format = "%(asctime)s [%(levelname)8s] %(message)s | %(filename)s:%(funcName)s:%(lineno)s"
log_cli_date_format = "%Y-%m-%d %H:%M:%S"

[tool.mypy]
python_version = "3.10"
exclude = ['mlcvzoo_service/tests']

junit_xml = "xunit-reports/xunit-result-mypy.xml"

# output style configuration
show_column_numbers = true
show_error_codes = true
pretty = true

# additional warnings
warn_return_any = true
warn_unused_configs = true
warn_unused_ignores = true
warn_redundant_casts = true
warn_no_return = true

no_implicit_optional = true
# unreachable code checking produces practically only false positives
warn_unreachable = false
disallow_untyped_defs = true
disallow_incomplete_defs = true
# disallow_any_explicit = true
disallow_any_generics = true
disallow_untyped_calls = true
ignore_missing_imports = false
# plugins = "numpy.typing.mypy_plugin"

# ignores that library has no typing information with it
[[tool.mypy.overrides]]
module = [
  "cv2", # https://github.com/opencv/opencv/issues/14590
  "cryptography.*",
  "docker.*",
  "flask_restx.*",
  "keras.*",
  "imageio.*",
  "keras_preprocessing.*",
  "mlflow.*",
  "mmcv.*",
  "mmdet.*",
  "mmocr.*",
  "numpy.*",
  "pandas.*",
  "PIL.*",
  "related.*",
  "scipy.*",
  "sklearn.*",
  "tensorflow.*",
  "terminaltables.*",
  "torch.*",
  "torchvision.*",
  "tqdm.*",
  "yolox.*"
]
ignore_missing_imports = true
