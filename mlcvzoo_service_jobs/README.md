# MLCVZoo Service Jobs

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_service_jobs** is a utility package for the mlcvzoo-service
to execute training or prediction jobs. While the mlcvzoo-service is a REST service to manage incoming
jobs, the mlcvzoo_service_jobs package is used to execute these jobs.

## Setup

NOTE: In order to use the GPU in docker container you need to setup the nvidia-container-runtime


## Run

The mlcvzoo-service-jobs module is intended to be used by the mlcvzoo-service. For execution build or pull the
mlcvzoo-service-jobs runtime image and tag it with a custom name e.g. `mlcvzoo-service-jobs-runtime:latest`.
Afterwards, specify the image name in the mlcvzoo-service configuration. The mlcvzoo-service will then use the
image to execute the jobs.


## Technology stack

- Python
