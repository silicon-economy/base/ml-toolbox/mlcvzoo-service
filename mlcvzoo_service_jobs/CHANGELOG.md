# MLCVZoo mlcvzoo_service_jobs module versions:

1.0.0 (2025-01-28)
------------------
- Rename package from mlcvzoo_service_runtime to mlcvzoo_service_jobs

0.1.0 (2024-12-18)
------------------
- Initial release of the package
