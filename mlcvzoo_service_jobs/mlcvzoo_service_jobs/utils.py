# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Utility methods for the mlcvzoo-service-runtime modules"""

import datetime
import logging

from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlflow import MlflowException

from mlcvzoo_service_jobs.jobs.constants import JobStatus

logger = logging.getLogger(__name__)


def log_status_to_mlflow(mlflow_adapter: MlflowAdapter, status: JobStatus) -> None:
    """Log the job status.

    Args:
        mlflow_adapter: The adapter for accessing the mlflow.
        status: The status of the job.
    """
    try:
        if not mlflow_adapter.credentials.run_id:
            logger.warning("Could not log status to mlflow, run_id is not set.")
            return

        mlflow_adapter.log_param(
            run_id=mlflow_adapter.credentials.run_id,
            key=str(status.value),
            value=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        )
    except MlflowException:
        # It is not allowed to change mlflow parameters therefore the status
        # can only be logged once
        pass
