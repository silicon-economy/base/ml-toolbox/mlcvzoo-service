# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Abstract base class of a job.
"""

import os
from abc import ABC
from typing import Any, Dict, List, Optional, Tuple

from mlcvzoo_base.configuration.model_config import ModelConfig
from mlcvzoo_base.models.model_registry import ModelRegistry
from mlcvzoo_util.adapters.s3.adapter import S3Adapter
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service_jobs.jobs.constants import JobStatus


class BaseJob(ABC):
    """
    Encapsulating a single base job.
    """

    def __init__(
        self,
        image_locations: List[S3DataLocation],
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        model_checkpoint_location: S3DataLocation,
    ):
        self.image_locations: List[S3DataLocation] = image_locations
        self.model_checkpoint_location = model_checkpoint_location

        self.string_replacement_map: Dict[
            str, str
        ] = self._init_string_replacement_map()

        self._model_registry: ModelRegistry = ModelRegistry()  # type: ignore[type-arg]
        self._model_configuration_dict: Dict[str, Any] = model_configuration_dict
        self.model_config: ModelConfig = self._build_model_config(
            model_type_name=model_type_name,
            model_configuration_dict=model_configuration_dict,
            string_replacement_map=self.string_replacement_map,
        )

        self._status: JobStatus = JobStatus.NEW
        self._results: Dict[str, Any] = {}

    @staticmethod
    def _init_string_replacement_map() -> Dict[str, str]:
        string_replacement_map = {}

        try:
            import mmocr

            string_replacement_map["MMOCR_DIR"] = os.path.join(
                os.path.dirname(mmocr.__file__), ".mim"
            )
        except ImportError:
            pass

        return string_replacement_map

    def _build_model_config(
        self,
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        string_replacement_map: Optional[Dict[str, str]] = None,
    ) -> ModelConfig:
        """Build the model configuration."""
        return self._model_registry.build_model_config(
            model_type_name=model_type_name,
            model_configuration_dict=model_configuration_dict,
            string_replacement_map=string_replacement_map,
        )

    @property
    def status(self) -> JobStatus:
        return self._status

    @status.setter
    def status(self, value: JobStatus) -> None:
        self._status = value

    def run(self) -> Dict[str, Any]:
        """Main function of the TrainingJob.
        By calling TrainingJob.start() it is executed in a Thread

        Returns:
            None
        """
        raise NotImplementedError("Must be implemented by subclass!")

    def _download_job_related_data(
        self, data: List[S3DataLocation]
    ) -> Tuple[Dict[str, str], List[str], List[str]]:
        """Download the necessary data to start a job.

        The necessary data is job related and therefore the data to download depends on the actual
        job.

        Args:
            data: Data to download. Data is represented by a list of Locations from where to
            download.

        Returns:
            A replacement map that contains the information about where exactly the data was
            downloaded to, a list of all downloaded files and a list of all original files.
        """
        string_replacement_map: Dict[str, str] = {}
        all_downloaded_files: List[str] = []
        all_original_files: List[str] = []

        for location in data:
            if isinstance(location, S3DataLocation):
                adapter = S3Adapter(credentials=location.credentials)

                (
                    string_replacement_map,
                    downloaded_files,
                    original_files,
                ) = adapter.download_objects(
                    bucket=location.bucket,
                    prefix=location.prefix,
                    location_id=location.location_id,
                    string_replacement_map=self.string_replacement_map,
                )
                all_downloaded_files.extend(downloaded_files)
                all_original_files.extend(original_files)

                self.string_replacement_map.update(string_replacement_map)
            else:
                raise KeyError("Storage type of class %s not supported", type(location))

        return string_replacement_map, all_downloaded_files, all_original_files
