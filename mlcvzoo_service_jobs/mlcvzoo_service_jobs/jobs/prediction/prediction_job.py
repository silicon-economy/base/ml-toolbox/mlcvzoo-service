# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Module defining a prediction job handling the inference of a given model.
"""

from __future__ import annotations

import logging
import os
from copy import deepcopy
from typing import Any, Dict, List, Optional, cast

import cv2
from mlcvzoo_base.api.configuration import ModelConfiguration
from mlcvzoo_base.api.data.data_registry import DataRegistry
from mlcvzoo_base.api.data.ocr_perception import OCRPerception
from mlcvzoo_base.api.interfaces import NetBased
from mlcvzoo_base.api.model import Model
from mlcvzoo_base.configuration.model_config import ModelConfig
from mlcvzoo_base.configuration.structs import OpenCVImageFormats
from mlcvzoo_base.models.model_registry import ModelRegistry
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service_jobs.jobs.base.base_job import BaseJob
from mlcvzoo_service_jobs.jobs.constants import JobStatus

logger = logging.getLogger(__name__)


class PredictionJob(BaseJob):
    """
    Encapsulates a single prediction job.
    """

    def __init__(
        self,
        image_locations: List[S3DataLocation],
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        model_checkpoint_location: S3DataLocation,
        second_model_type_name: str | None = None,
        second_model_configuration_dict: Dict[str, Any] | None = None,
        second_model_checkpoint_location: S3DataLocation | None = None,
    ):
        BaseJob.__init__(
            self,
            image_locations=image_locations,
            model_type_name=model_type_name,
            model_configuration_dict=model_configuration_dict,
            model_checkpoint_location=model_checkpoint_location,
        )

        self.second_model_config: Optional[ModelConfig] = None
        self.second_model_checkpoint_location: Optional[
            S3DataLocation
        ] = second_model_checkpoint_location
        if (
            second_model_type_name is not None
            and second_model_configuration_dict is not None
        ):
            self.second_model_config = self._build_model_config(
                model_type_name=second_model_type_name,
                model_configuration_dict=second_model_configuration_dict,
                string_replacement_map=self.string_replacement_map,
            )

        self.__data_registry = DataRegistry()

    def run(self) -> Dict[str, Any]:
        """Main function of the PredictionJob."""
        self.status = JobStatus.RUNNING
        logger.info(f"Start of the prediction job!")
        try:
            data = [self.model_checkpoint_location] + self.image_locations
            if self.second_model_checkpoint_location is not None:
                data.append(self.second_model_checkpoint_location)
            (
                string_replacement_map,
                downloaded_files,
                original_files,
            ) = self._download_job_related_data(data=data)
            image_files: Dict[str, str] = {}
            accepted_extensions = {
                OpenCVImageFormats.JPG.value,
                OpenCVImageFormats.JPEG.value,
                OpenCVImageFormats.PNG.value,
            }
            for downloaded_file, original_file in zip(downloaded_files, original_files):
                file_extension = os.path.splitext(downloaded_file)[1].lower()
                if file_extension in accepted_extensions:
                    image_files[original_file] = downloaded_file

            self.__predict(images=image_files)

            self.status = JobStatus.FINISHED
        except Exception as e:
            logger.exception(e)
            self._results["crashed"] = True
            self._results["message"] = str(e)

            self.status = JobStatus.CRASHED

        return self._results

    @staticmethod
    def __init_model(
        model_config: ModelConfig,
        model_checkpoint_location: S3DataLocation,
        string_replacement_map: Dict[str, str],
    ) -> Model:  # type: ignore[type-arg]
        # If the config is not already configured for inference do it now
        if "init_for_inference" not in model_config.constructor_parameters:
            model_config.constructor_parameters["init_for_inference"] = True

        cast(
            ModelConfiguration,
            model_config.constructor_parameters["configuration"],
        ).recursive_string_replacement(string_replacement_map=string_replacement_map)

        registry: ModelRegistry = ModelRegistry()  # type: ignore[type-arg]

        model = registry.init_model(
            model_config=model_config,
            string_replacement_map=string_replacement_map,
        )

        if isinstance(model, NetBased):
            model_checkpoint = os.path.join(
                string_replacement_map[model_checkpoint_location.location_id],
                model_checkpoint_location.bucket,
                model_checkpoint_location.prefix,
            )
            model.restore(checkpoint_path=model_checkpoint)

        return model

    def __predict(self, images: Dict[str, Any]) -> None:
        """Predict all images of the job with the given model.

        Args:
            exactly the images and the model are stored.
            images: Dict where each element represents the original image path (key) and the
            download path (value).
        """

        model = self.__init_model(
            model_config=self.model_config,
            model_checkpoint_location=self.model_checkpoint_location,
            string_replacement_map=self.string_replacement_map,
        )

        second_model: Optional[Model] = None  # type: ignore[type-arg]
        if self.second_model_config and self.second_model_checkpoint_location:
            second_model = self.__init_model(
                model_config=self.second_model_config,
                model_checkpoint_location=self.second_model_checkpoint_location,
                string_replacement_map=self.string_replacement_map,
            )

        if second_model is not None:
            logger.info(
                "Predict on crops with second model unique-name=%s",
                second_model.unique_name,
            )

        for original_path, image_path in images.items():
            _, predictions = model.predict(data_item=image_path)

            logger.debug(f"Predictions for {original_path}: {predictions}")

            if second_model is not None:
                for prediction in predictions:
                    frame = cv2.imread(image_path)
                    cropped_image = prediction.crop_img_by_ratio(
                        image=deepcopy(frame),
                        padding_ratio=0.1,
                    )

                    _, second_predictions = second_model.predict(
                        data_item=cropped_image
                    )

                    logger.debug(
                        f"Second-Predictions for {original_path}: {second_predictions}"
                    )

                    # TODO: Filter for OCRPerception with max score?
                    if second_predictions and isinstance(
                        second_predictions[0], OCRPerception
                    ):
                        prediction.content = second_predictions[0].content

            if predictions:
                self._results[original_path] = [
                    self.__data_registry.encode(prediction=prediction)
                    for prediction in predictions
                ]
