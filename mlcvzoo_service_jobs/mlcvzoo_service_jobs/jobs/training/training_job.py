# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Encapsulates a single training job in the mlcvzoo-service.
"""

from __future__ import annotations

import logging
import os
from typing import Any, Dict, List, Optional

import cv2
from config_builder import BaseConfigClass
from mlcvzoo_base.api.data.types import MetricInfo
from mlcvzoo_base.api.interfaces import LogProvider, MetricProvider, Trainable
from mlcvzoo_base.api.model import Model
from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mlcvzoo_base.configuration.model_config import ModelConfig
from mlcvzoo_base.evaluation.geometric.configuration import TensorboardLoggingConfig
from mlcvzoo_base.evaluation.geometric.data_classes import GeometricEvaluationMetrics
from mlcvzoo_base.evaluation.geometric.metrics_logging import (
    log_od_metrics_to_mlflow_run,
)
from mlcvzoo_base.evaluation.geometric.utils import create_fp_fn_images
from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation
from mlcvzoo_util.model_evaluator.configuration import (
    CheckpointConfig,
    ModelEvaluatorCLIConfig,
)
from mlcvzoo_util.model_evaluator.metric_factory import (
    GeometricMetricFactory,
    get_factory,
)
from mlcvzoo_util.model_evaluator.model_evaluator import run_evaluation
from mlcvzoo_util.model_evaluator.structs import CheckpointInfo
from related import to_model

from mlcvzoo_service_jobs.jobs.base.base_job import BaseJob
from mlcvzoo_service_jobs.jobs.constants import JobStatus
from mlcvzoo_service_jobs.utils import log_status_to_mlflow

logger = logging.getLogger(__name__)


class TrainingJob(BaseJob):
    """
    Encapsulates a single training job.
    """

    TRAINING_OUTPUT_DIR: str = "TRAINING_OUTPUT_DIR"
    TENSORBOARD_DIR: str = "TENSORBOARD_DIR"

    def __init__(
        self,
        image_locations: List[S3DataLocation],
        annotation_locations: List[S3DataLocation],
        model_checkpoint_location: S3DataLocation,
        model_type_name: str,
        model_configuration_dict: Dict[str, Any],
        mlflow_adapter: MlflowAdapter,
        evaluator_configuration_dict: Optional[Dict[str, Any]] = None,
    ):
        BaseJob.__init__(
            self,
            image_locations=image_locations,
            model_type_name=model_type_name,
            model_configuration_dict=model_configuration_dict,
            model_checkpoint_location=model_checkpoint_location,
        )

        self.annotation_locations: List[S3DataLocation] = annotation_locations

        self._mlflow_adapter: MlflowAdapter = mlflow_adapter

        # The .evaluator_configuration is only needed for GeometricModel evaluation
        self.evaluator_configuration: Optional[ModelEvaluatorCLIConfig] = None
        if evaluator_configuration_dict is not None:
            self.evaluator_configuration = TrainingJob.build_model_evaluator_config(
                model_config=self.model_config,
                evaluator_configuration_dict=evaluator_configuration_dict,
            )

    @staticmethod
    def _init_string_replacement_map() -> Dict[str, str]:
        string_replacement_map = BaseJob._init_string_replacement_map()

        string_replacement_map[TrainingJob.TRAINING_OUTPUT_DIR] = os.getenv(
            TrainingJob.TRAINING_OUTPUT_DIR, os.getcwd()
        )

        return string_replacement_map

    @property
    def status(self) -> JobStatus:
        return super().status

    @status.setter
    def status(self, value: JobStatus) -> None:
        self._status = value
        log_status_to_mlflow(mlflow_adapter=self._mlflow_adapter, status=value)

    @staticmethod
    def build_model_evaluator_config(
        model_config: ModelConfig,
        evaluator_configuration_dict: Dict[str, Any],
    ) -> ModelEvaluatorCLIConfig:
        return ModelEvaluatorCLIConfig(
            iou_thresholds=evaluator_configuration_dict["iou_thresholds"],
            model_config=model_config,
            annotation_handler_config=to_model(
                cls=AnnotationHandlerConfig,
                value=evaluator_configuration_dict["annotation_handler_config"],
            ),
            tensorboard_logging_config=TensorboardLoggingConfig(
                tensorboard_dir=os.getenv(TrainingJob.TENSORBOARD_DIR, os.getcwd()),
                # TODO: Make configurable?
                false_positive_image_size=750,
            ),
        )

    def run(self) -> Dict[str, Any]:
        """
        Main function of the TrainingJob.

        Returns:
            The results of the job.
        """
        self.status = JobStatus.RUNNING
        logger.info(
            "Start TrainingJob: mlflow run-id='%s'",
            self._mlflow_adapter.credentials.run_id,
        )

        try:
            self.string_replacement_map.update(
                self._download_job_related_data(
                    data=(
                        [self.model_checkpoint_location]
                        + self.image_locations
                        + self.annotation_locations
                    )
                )[0]
            )
            self.__train()

            self.status = JobStatus.FINISHED
        except Exception as e:
            logger.exception(e)
            self._results["crashed"] = True
            self._results["message"] = str(e)

            self.status = JobStatus.CRASHED

        return self._results

    def __log_best_metric(self, best_checkpoint_info: CheckpointInfo) -> None:
        """
        Log the best metric together with the associated checkpoint to mlflow

        Args:
            best_checkpoint_info: Object containing the best checkpoint information

        Returns:
            None
        """
        if not self._mlflow_adapter.credentials.run_id:
            logger.warning(
                "No mlflow run_id available. Skipping logging of best metric."
            )
            return None

        logger.debug(f"Best checkpoint: {best_checkpoint_info}")

        self._mlflow_adapter.log_param(
            run_id=self._mlflow_adapter.credentials.run_id,
            key="best_checkpoint",
            value=os.path.basename(best_checkpoint_info.path),
        )
        self._mlflow_adapter.log_metric(
            run_id=self._mlflow_adapter.credentials.run_id,
            key="score",
            value=best_checkpoint_info.score,
        )
        self._mlflow_adapter.log_artifact(
            run_id=self._mlflow_adapter.credentials.run_id,
            local_path=best_checkpoint_info.path,
        )

    def __log_geometric_evaluation_results(
        self,
        evaluated_checkpoint_metrics: Dict[str, Any],
        best_checkpoint_info: CheckpointInfo,
    ) -> None:
        """
        Log geometric evaluation results to mlflow.
        This includes the metrics and the false-positive and false-negative evaluation images

        Args:
            evaluated_checkpoint_metrics: Dictionary of evaluated checkpoint metrics
            best_checkpoint_info: CheckpointInfo of the best checkpoint
        """
        if not self._mlflow_adapter.credentials.run_id:
            logger.warning("No mlflow run_id available. Skipping logging of metrics.")
            return None

        # Log False-Positive and False-Negative images only for the best checkpoint,
        # since this checkpoint will be registered to be used for model deployment
        best_metrics = evaluated_checkpoint_metrics[best_checkpoint_info.path]

        if isinstance(best_metrics, GeometricEvaluationMetrics):
            fp_fn_image_dict = create_fp_fn_images(
                metrics_image_info_dict=best_metrics.metrics_image_info_dict
            )
            for (
                fp_fn_image_name,
                class_identifier_images,
            ) in fp_fn_image_dict.items():
                fp_fn_image_name = fp_fn_image_name.split("/")[-1]

                for class_identifier_str, image in class_identifier_images.items():
                    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

                    self._mlflow_adapter.log_image(
                        run_id=self._mlflow_adapter.credentials.run_id,
                        image=image,
                        artifact_file=os.path.join(
                            "fp_fn_images", class_identifier_str, fp_fn_image_name
                        ),
                    )

        # Logging for metric history
        for step, (ckpt, metrics) in enumerate(evaluated_checkpoint_metrics.items()):
            if isinstance(metrics, GeometricEvaluationMetrics):
                # best_checkpoint_step is used as indicator for the epoch for
                # which we have logged the best_checkpoint as artifact. Later
                # on this step can be used to search for the correct metrics
                # in the logged mlflow metrics.
                if ckpt == best_checkpoint_info.path:
                    self._mlflow_adapter.log_param(
                        run_id=self._mlflow_adapter.credentials.run_id,
                        key="best_checkpoint_step",
                        value=step,
                    )

                for iou in metrics.metrics_dict:
                    log_od_metrics_to_mlflow_run(
                        mlflow_client=self._mlflow_adapter.client,
                        run_id=self._mlflow_adapter.credentials.run_id,
                        model_specifier=metrics.model_specifier,
                        metrics_dict=metrics.metrics_dict,
                        iou_threshold=float(iou),
                        step=step,
                    )

    def __init_model(self) -> Model:  # type: ignore[type-arg]
        self.model_config.set_inference(inference=False)

        if "configuration" in self.model_config.constructor_parameters and isinstance(
            self.model_config.constructor_parameters["configuration"], BaseConfigClass
        ):
            self.model_config.constructor_parameters[
                "configuration"
            ].recursive_string_replacement(
                string_replacement_map=self.string_replacement_map
            )

        return self._model_registry.init_model(
            model_config=self.model_config,
            string_replacement_map=self.string_replacement_map,
        )

    def __evaluate(self, model: Model) -> None:  # type: ignore[type-arg]
        evaluated_checkpoint_metrics: Dict[str, Any]
        best_checkpoint_info: Optional[CheckpointInfo] = None

        metric_factory_class = get_factory(inference_model=model)

        if (
            metric_factory_class is GeometricMetricFactory
            and self.evaluator_configuration
        ):
            self.evaluator_configuration.recursive_string_replacement(
                string_replacement_map=self.string_replacement_map
            )
            self.evaluator_configuration.checkpoint_config = CheckpointConfig(
                checkpoint_dir=model.get_training_output_dir(),  # type: ignore[attr-defined]
                checkpoint_filename_suffix=model.get_checkpoint_filename_suffix(),  # type: ignore[attr-defined]
            )
            evaluated_checkpoint_metrics, best_checkpoint_info = run_evaluation(
                configuration=self.evaluator_configuration,
                single_mode=False,
            )
            logger.debug(f"Best metric: {best_checkpoint_info}")

            self.__log_geometric_evaluation_results(
                evaluated_checkpoint_metrics=evaluated_checkpoint_metrics,
                best_checkpoint_info=best_checkpoint_info,
            )
        elif isinstance(model, MetricProvider):
            (
                best_metric,
                evaluated_checkpoint_metrics,
            ) = model.determine_training_metrics()

            if isinstance(best_metric, MetricInfo):
                logger.debug(f"Best metric: {best_metric}")
                best_checkpoint_info = CheckpointInfo(
                    path=best_metric.path, score=best_metric.score
                )
        else:
            raise ValueError(
                f"Neither a metric factory nor a MetricProvider found "
                f"for the given model type={type(model)}.",
            )

        if not best_checkpoint_info or (
            best_checkpoint_info.path == "model_state"
            and best_checkpoint_info.score == 0.0
        ):
            raise RuntimeError(
                "No checkpoint was saved during training or could be successfully evaluated. "
                "Training logs will be saved anyway. Check the logs for more details."
            )

        self.__log_best_metric(
            best_checkpoint_info=best_checkpoint_info,
        )

        if isinstance(model, LogProvider) and self._mlflow_adapter.credentials.run_id:
            logs: Dict[int, Dict[str, Any]] = model.determine_training_logs()

            for epoch, log_entries in logs.items():
                for log_name, log_value in log_entries.items():
                    self._mlflow_adapter.log_metric(
                        run_id=self._mlflow_adapter.credentials.run_id,
                        key=log_name,
                        value=log_value,
                        step=epoch,
                    )

    def __train(self) -> None:
        # Log the original configuration dictionary rather than the one from the attribute
        # model_config. In mlflow we want to have the generic model configuration, without
        # having the string replacement placeholders / environment variables replaced. This
        # ensures to have a generic model configuration that can be deployed to any device
        # with the correct placeholder / environment settings.
        if self._mlflow_adapter.credentials.run_id is not None:
            self._mlflow_adapter.log_model_configuration_dict(
                run_id=self._mlflow_adapter.credentials.run_id,
                configuration_dict=self._model_configuration_dict,
            )

        model = self.__init_model()

        # Log relevant model parameters as soon as they are available
        if self._mlflow_adapter.credentials.run_id:
            self._mlflow_adapter.log_param(
                run_id=self._mlflow_adapter.credentials.run_id,
                key="algorithm_type",
                value=model.algorithm_type_name,
            )

        if not isinstance(model, Trainable):
            raise ValueError("Model is not trainable or net-based.")

        logger.info("Start training of model unique-name=%s", model.unique_name)
        model.train()

        self.__evaluate(model=model)

        del model
