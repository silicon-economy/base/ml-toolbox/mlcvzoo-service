# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Entrypoint for the mlcvzoo-service-jobs module"""

import logging

from mlcvzoo_service_jobs.runner import Runner

logger = logging.getLogger(__name__)


def main() -> None:
    logging.basicConfig(
        format="%(asctime)s %(levelname)-7.7s: "
        "[%(name)-30.30s]"
        "[%(threadName)-11.11s]"
        "[%(filename)s:%(lineno)s - %(funcName)20s()] "
        "%(message)s",
        level=logging.DEBUG,
    )

    runner = Runner()
    runner.run()


if __name__ == "__main__":
    main()
