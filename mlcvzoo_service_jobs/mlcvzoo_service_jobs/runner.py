# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Implement the Runner class to execute different jobs."""

import json
import logging
import os
from typing import Any, Dict, Optional, Union

import requests
from config_builder.json_encoding import TupleEncoder
from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service_jobs.jobs.constants import (
    JobStatus,
    JobTypes,
    PredictionJobParameters,
    SharedJobParameters,
    TrainingJobParameters,
)
from mlcvzoo_service_jobs.jobs.prediction.prediction_job import PredictionJob
from mlcvzoo_service_jobs.jobs.training.training_job import TrainingJob
from mlcvzoo_service_jobs.utils import log_status_to_mlflow

logger = logging.getLogger(__name__)


class Runner:
    """Encapsulate the execution of different jobs."""

    def __init__(self) -> None:
        """Initialize the Runner."""
        self.string_replacement_map: Dict[str, str] = {}

        self.job_type = Runner.get_credential_constant(SharedJobParameters.JOB_TYPE)

        if self.job_type == JobTypes.TRAINING.value:
            self._mlflow_adapter = self.__setup_mlflow()

    @staticmethod
    def get_credential_constant(const_name: str) -> str:
        """Return value for a credential constant from the os environment variables.

        Args:
            const_name: Environment variable key for the constant.

        Raises:
            ValueError: If no value for a credential constant can be found in the environment variables.

        Returns:
            The value for a given constant key in the environment variables.
        """
        value = os.environ.get(const_name)
        if value is None:
            raise ValueError(
                f"Necessary environment variable '{const_name}' not been set!"
            )
        return value

    @staticmethod
    def __post_results(results: Dict[str, Any], job_manager_endpoint: str) -> None:
        """Post the results to the job manager.
        This is used as a callback to indicate that the job has finished its execution.

        Args:
            results: The results of the job.
        """
        endpoint = job_manager_endpoint

        response = requests.put(url=endpoint, json=results)

        if response.status_code == 200:
            logger.info("Results posted successfully.")
        else:
            logger.error(
                f"Results could not be posted. Status code: {response.status_code}"
            )

    @staticmethod
    def __setup_mlflow() -> MlflowAdapter:
        """Setup Mlflow for tracking"""

        mlflow_credentials_dict = Runner.get_credential_constant(
            TrainingJobParameters.MLFLOW_CREDENTIALS
        )

        mlflow_credentials = MlflowCredentials(
            **json.loads(mlflow_credentials_dict),
        )
        mlflow_credentials.update_os_environment()

        return MlflowAdapter(mlflow_credentials)

    def run(self) -> None:
        """Interface of the Runner.

        This method is called to start the job.
        """
        results = {}
        job_manager_endpoint = ""

        try:
            job_manager_endpoint = Runner.get_credential_constant(
                SharedJobParameters.SERVICE_ENDPOINT
            )

            images_locations = Runner.get_credential_constant(
                SharedJobParameters.IMAGE_LOCATIONS
            )

            model_checkpoint_location = Runner.get_credential_constant(
                SharedJobParameters.MODEL_CHECKPOINT_LOCATION
            )
            model_type_name = Runner.get_credential_constant(
                SharedJobParameters.MODEL_TYPE_NAME
            )
            model_configuration_dict = Runner.get_credential_constant(
                SharedJobParameters.MODEL_CONFIGURATION_DICT
            )

            job: Union[TrainingJob, PredictionJob]
            if self.job_type == JobTypes.TRAINING.value:
                evaluator_configuration_dict_string = Runner.get_credential_constant(
                    TrainingJobParameters.EVAL_CONFIGURATION_DICT
                )

                evaluator_configuration_dict: Optional[Dict[str, Any]] = None
                if evaluator_configuration_dict_string:
                    evaluator_configuration_dict = TupleEncoder.decode(
                        json.loads(evaluator_configuration_dict_string)
                    )

                annotation_locations = Runner.get_credential_constant(
                    TrainingJobParameters.ANNOTATION_LOCATIONS
                )

                job = TrainingJob(
                    image_locations=[
                        S3DataLocation(**image_location)
                        for image_location in json.loads(images_locations)
                    ],
                    annotation_locations=[
                        S3DataLocation(**annotation)
                        for annotation in json.loads(annotation_locations)
                    ],
                    model_checkpoint_location=S3DataLocation(
                        **json.loads(model_checkpoint_location)
                    ),
                    model_type_name=model_type_name,
                    model_configuration_dict=TupleEncoder.decode(
                        json.loads(model_configuration_dict)
                    ),
                    evaluator_configuration_dict=evaluator_configuration_dict,
                    mlflow_adapter=self._mlflow_adapter,
                )
                results = job.run()

            elif self.job_type == JobTypes.PREDICTION.value:
                second_model_location = os.environ.get(
                    PredictionJobParameters.SECOND_MODEL_CHECKPOINT_LOCATION
                )
                second_model_type_name = os.environ.get(
                    PredictionJobParameters.SECOND_MODEL_TYPE_NAME
                )
                second_model_configuration_dict = os.environ.get(
                    PredictionJobParameters.SECOND_MODEL_CONFIGURATION_DICT
                )

                if (
                    second_model_location is not None
                    and second_model_type_name is not None
                    and second_model_configuration_dict is not None
                ):
                    job = PredictionJob(
                        image_locations=[
                            S3DataLocation(**image)
                            for image in json.loads(images_locations)
                        ],
                        model_checkpoint_location=S3DataLocation(
                            **json.loads(model_checkpoint_location)
                        ),
                        model_type_name=model_type_name,
                        model_configuration_dict=TupleEncoder.decode(
                            json.loads(model_configuration_dict)
                        ),
                        second_model_checkpoint_location=S3DataLocation(
                            **json.loads(second_model_location)
                        ),
                        second_model_type_name=second_model_type_name,
                        second_model_configuration_dict=TupleEncoder.decode(
                            json.loads(second_model_configuration_dict)
                        ),
                    )
                else:
                    job = PredictionJob(
                        image_locations=[
                            S3DataLocation(**image)
                            for image in json.loads(images_locations)
                        ],
                        model_checkpoint_location=S3DataLocation(
                            **json.loads(model_checkpoint_location)
                        ),
                        model_type_name=model_type_name,
                        model_configuration_dict=TupleEncoder.decode(
                            json.loads(model_configuration_dict)
                        ),
                    )
                results = job.run()
            else:
                raise ValueError(
                    f"Job type must one of {[job_type for job_type in JobTypes]}"
                )

        except Exception as e:
            # TODO: Better interface for error handling
            results = dict()
            results["message"] = str(e)
            results["crashed"] = True
            # if the initialization of TrainingJob fails, the state must be set to CRASHED
            if self.job_type == JobTypes.TRAINING.value:
                log_status_to_mlflow(
                    mlflow_adapter=self._mlflow_adapter,
                    status=JobStatus.CRASHED,
                )

            logger.exception(e)
        finally:
            # In case of an error
            self.__post_results(
                results=results, job_manager_endpoint=job_manager_endpoint
            )
