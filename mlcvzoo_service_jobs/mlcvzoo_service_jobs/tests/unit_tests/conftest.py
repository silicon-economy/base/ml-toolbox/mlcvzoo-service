# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Define common fixtures for the unit tests."""

import os
from logging import getLogger
from pathlib import Path

import pytest
from mlcvzoo_util.adapters.s3.credentials import S3Credentials

import mlcvzoo_service_jobs

logger = getLogger(__name__)


def determine_project_root() -> Path:
    """Provide the project root path."""

    this_dir = Path(os.path.dirname(os.path.abspath(__file__))).resolve()

    setup_path = this_dir
    while setup_path.exists() and setup_path.name != mlcvzoo_service_jobs.__name__:
        if setup_path == setup_path.parent:
            raise RuntimeError("Could not find setup_path!")
        else:
            setup_path = setup_path.parent
    # One more to be above the target directory
    setup_path = setup_path.parent

    return setup_path


@pytest.fixture(name="project_root")
def project_root_fixture() -> Path:
    """Provide the project root path as fixture."""

    return determine_project_root()


@pytest.fixture(name="s3_credentials")
def s3_credentials():
    """Get S3 credentials."""
    return S3Credentials(
        s3_artifact_access_id="testuser",
        s3_artifact_access_key="123456",
        s3_region_name="eu-central-1",
        s3_endpoint_url="http://test-s3:9000",
    )
