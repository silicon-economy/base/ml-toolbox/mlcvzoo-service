# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
import logging
import os
from typing import Dict, List, Optional, Tuple
from unittest import mock

import related
from attrs import define
from mlcvzoo_base.api.configuration import ModelConfiguration
from mlcvzoo_base.api.data.annotation_class_mapper import AnnotationClassMapper
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.interfaces import NetBased, Trainable
from mlcvzoo_base.api.model import DataType, ObjectDetectionModel
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

from mlcvzoo_service_jobs.jobs.prediction.prediction_job import PredictionJob
from mlcvzoo_service_jobs.tests.unit_tests.test_template import TestTemplate

logger = logging.getLogger(__name__)

DUMMY_BOUNDING_BOX = BoundingBox(
    box=mock.MagicMock(),
    class_identifier=mock.MagicMock(),
    score=0.42,
    difficult=False,
    occluded=False,
    content="",
)


@define
class TestConfig(ModelConfiguration):
    test_config: str = related.StringField(required=False, default="Hello World")


class TestModel(ObjectDetectionModel, NetBased, Trainable):
    def __init__(
        self,
        from_yaml: Optional[str] = None,
        configuration: Optional[ModelConfiguration] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
        init_for_inference: bool = True,
    ):
        self.configuration = TestConfig(unique_name="test_model")

        ObjectDetectionModel.__init__(
            self,
            configuration=self.configuration,
            mapper=AnnotationClassMapper(
                class_mapping=ClassMappingConfig(
                    model_classes=[
                        ClassMappingModelClassesConfig(
                            class_name="test_class", class_id=0
                        )
                    ],
                    number_model_classes=1,
                ),
            ),
            init_for_inference=True,
        )

        NetBased.__init__(self, net=None)
        Trainable.__init__(self)

    def get_training_output_dir(self) -> Optional[str]:
        return ""

    def predict(self, data_item: DataType) -> Tuple[DataType, List[BoundingBox]]:
        return data_item, [DUMMY_BOUNDING_BOX]

    def restore(self, checkpoint_path: str) -> None:
        pass

    def train(self) -> None:
        pass

    def store(self, checkpoint_path: str) -> None:
        pass

    @staticmethod
    def create_configuration(
        from_yaml: Optional[str] = None,
        configuration: Optional[TestConfig] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
    ) -> TestConfig:
        return TestConfig(unique_name="test-model")


@mock.patch(
    "mlcvzoo_service_jobs.jobs.prediction.prediction_job.ModelRegistry.init_model"
)
@mock.patch(
    "mlcvzoo_service_jobs.jobs.prediction.prediction_job.ModelRegistry"
    ".determine_config_class"
)
@mock.patch("mlcvzoo_util.adapters.s3.adapter.S3Adapter.download_objects")
def test_run_job(mock_download, mock_determine, mock_init, s3_credentials):
    """Test if a prediction job is successfully run."""

    # set mock return values
    mock_init.return_value = TestModel()
    mock_determine.return_value = TestConfig
    test_map = {"TEST_STRING": "test", "TEST_OUTPUT": "test/output_dir"}
    original_image_path = "/remote/original_dir/original_test_image.jpg"
    downloaded_image_path = "/local/download_dir/downloaded_test_image.jpg"
    mock_download.return_value = (
        test_map,
        [downloaded_image_path],
        [original_image_path],
    )

    prediction_job = PredictionJob(
        image_locations=[
            S3DataLocation(
                uri="https://127.0.0.1:9000/test-bucket/images",
                location_id="TEST_OUTPUT",
                credentials=s3_credentials,
            )
        ],
        model_checkpoint_location=S3DataLocation(
            uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
            location_id="TEST_OUTPUT",
            credentials=s3_credentials,
        ),
        model_type_name="test_model",
        model_configuration_dict=TestConfig(unique_name="test-model").to_dict(),
    )

    results = prediction_job.run()

    # job should have the expected BoundingBox for the original image path as prediction result
    assert {
        original_image_path: [
            {"data": DUMMY_BOUNDING_BOX.to_dict(), "type": "BoundingBox"}
        ]
    } == results


@mock.patch(
    "mlcvzoo_service_jobs.jobs.prediction.prediction_job.ModelRegistry.init_model"
)
@mock.patch(
    "mlcvzoo_service_jobs.jobs.prediction.prediction_job.ModelRegistry"
    ".determine_config_class"
)
@mock.patch("mlcvzoo_util.adapters.s3.adapter.S3Adapter.download_objects")
def test_run_job_fails(mock_download, mock_determine, mock_init, s3_credentials):
    """Test if a crashed status is returned as result if the job fails."""

    def _job_finished(param) -> None:
        logger.info("Job finished")

    mock_init.return_value = TestModel()
    mock_determine.return_value = TestConfig
    mock_download.side_effect = Exception

    prediction_job = PredictionJob(
        image_locations=[
            S3DataLocation(
                uri="https://127.0.0.1:9000/test-bucket/images",
                location_id="TEST_OUTPUT",
                credentials=s3_credentials,
            )
        ],
        model_checkpoint_location=S3DataLocation(
            uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
            location_id="TEST_OUTPUT",
            credentials=s3_credentials,
        ),
        model_type_name="test_model",
        model_configuration_dict=TestConfig(unique_name="test-model").to_dict(),
    )

    results = prediction_job.run()

    # job should be crashed
    assert results["crashed"] is True
