# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import copy
import logging
import os
import shutil
import unittest
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple
from unittest.mock import MagicMock, call

import numpy as np
import related
from attrs import define
from mlcvzoo_base.api.configuration import ModelConfiguration
from mlcvzoo_base.api.data.annotation_class_mapper import AnnotationClassMapper
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.types import ImageType
from mlcvzoo_base.api.interfaces import NetBased, Trainable
from mlcvzoo_base.api.model import ObjectDetectionModel
from mlcvzoo_base.configuration.annotation_handler_config import (
    AnnotationHandlerConfig,
    AnnotationHandlerPASCALVOCInputDataConfig,
)
from mlcvzoo_base.configuration.class_mapping_config import (
    ClassMappingConfig,
    ClassMappingModelClassesConfig,
)
from mlcvzoo_base.evaluation.geometric.data_classes import GeometricEvaluationMetrics
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation
from mlcvzoo_util.model_evaluator.structs import CheckpointInfo
from pytest import fixture, mark
from pytest_mock import MockerFixture

from mlcvzoo_service_jobs.jobs.training.training_job import TrainingJob

logger = logging.getLogger(__name__)


@define
class TestConfig(ModelConfiguration):
    test_config: str = related.StringField(required=False, default="Hello World")
    test_work_dir: str = related.StringField(required=False, default="${MEDIA_DIR}")


class TestModel(ObjectDetectionModel[TestConfig, ImageType], NetBased, Trainable):
    def __init__(self, *args, **kwargs):
        self.configuration = TestConfig(unique_name="test_model")

        ObjectDetectionModel.__init__(
            self,
            configuration=self.configuration,
            mapper=AnnotationClassMapper(
                class_mapping=ClassMappingConfig(
                    model_classes=[
                        ClassMappingModelClassesConfig(
                            class_name="test_class", class_id=0
                        )
                    ],
                    number_model_classes=1,
                ),
            ),
            init_for_inference=True,
        )

        NetBased.__init__(self, net=None)
        Trainable.__init__(self)

    def get_checkpoint_filename_suffix(self) -> str:
        return ""

    def get_training_output_dir(self) -> Optional[str]:
        return ""

    def predict(self, data_item: ImageType) -> Tuple[ImageType, List[BoundingBox]]:
        return data_item, []

    @property
    def num_classes(self) -> int:
        return 1

    def get_classes_id_dict(self) -> Dict[int, str]:
        return {}

    @staticmethod
    def create_configuration(
        from_yaml: Optional[str] = None,
        configuration: Optional[TestConfig] = None,
        string_replacement_map: Optional[Dict[str, str]] = None,
    ) -> TestConfig:
        return TestConfig(unique_name="test-model")

    def restore(self, checkpoint_path: str) -> None:
        pass

    def train(self) -> None:
        pass

    def store(self, checkpoint_path: str) -> None:
        pass


@fixture(scope="function")
def create_model_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.model_trainer.model_trainer.ModelTrainer.create_model",
        return_value=TestModel(),
    )


@fixture(scope="function")
def create_model_registry_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.model_evaluator.model_evaluator.ModelEvaluator.create_model",
        return_value=TestModel(),
    )


@fixture(scope="function")
def run_evaluation_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_service_jobs.jobs.training.training_job.run_evaluation",
        return_value=({}, CheckpointInfo(path="test_model.pth", score=1.0)),
    )


@fixture(scope="function")
def determine_config_class_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_base.models.model_registry.ModelRegistry.determine_config_class",
        return_value=TestConfig,
    )


@fixture(scope="function")
def get_model_type_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_base.models.model_registry.ModelRegistry.get_model_type",
        return_value=TestModel,
    )


@fixture(scope="function")
def download_objects_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.adapters.s3.adapter.S3Adapter.download_objects",
        return_value=({}, [], []),
    )


@fixture(scope="function")
def upload_objects_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.adapters.s3.adapter.S3Adapter.upload_file",
        return_value=None,
    )


@fixture(scope="function")
def mlflow_active_run_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlflow.active_run",
        return_value=None,
    )


@fixture(scope="function")
def mlflow_adapter_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("mlcvzoo_util.adapters.mlflow.adapter.MlflowAdapter")


@fixture(scope="function")
def mlflow_adapter_mock_return_value_none(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.adapters.mlflow.adapter.MlflowAdapter", return_value=None
    )


@fixture(scope="function")
def mlflow_tracking_client_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch("mlflow.tracking.client.TrackingServiceClient")


@fixture(scope="function")
def log_od_metrics_to_mlflow_run_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_base.evaluation.geometric.metrics_logging.log_od_metrics_to_mlflow_run"
    )


@fixture(scope="function")
def dummy_image() -> np.ndarray:
    return np.zeros([100, 100, 3], dtype=np.uint8)


@fixture(scope="function")
def fp_fn_image_dict(dummy_image: np.ndarray) -> Dict[str, Dict[str, np.ndarray]]:
    return {
        "fp_fn_img_path_1_ID_1_GT_1_FP_1_FN_1.jpeg": {
            "0_class": dummy_image,
            "1_class": dummy_image,
        },
        "fp_fn_img_path_2_ID_2_GT_1_FP_1_FN_1.jpg": {
            "0_class": dummy_image,
            "1_class": dummy_image,
        },
    }


@fixture(scope="function")
def create_fp_fn_images_mock(
    mocker: MockerFixture,
    dummy_image: np.ndarray,
    fp_fn_image_dict: Dict[str, Dict[str, np.ndarray]],
) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_base.evaluation.geometric.utils.create_fp_fn_images",
        return_value=fp_fn_image_dict,
    )


@mark.usefixtures(
    "create_model_mock",
    "determine_config_class_mock",
    "create_model_registry_mock",
    "download_objects_mock",
    "upload_objects_mock",
    "mlflow_active_run_mock",
    "mlflow_tracking_client_mock",
    "create_fp_fn_images_mock",
    "get_model_type_mock",
)
@unittest.mock.patch("mlflow.set_tracking_uri")
@unittest.mock.patch("mlflow.start_run")
@unittest.mock.patch("mlflow.end_run")
@unittest.mock.patch("time.sleep")
@unittest.mock.patch("mlcvzoo_service_jobs.jobs.training.training_job.MlflowAdapter")
def test_training_job_constructor(
    mlflow_set_tracking_uri_mock,
    mlflow_start_run_mock,
    mlflow_end_run_mock,
    time_sleep_mock,
    mlflow_adapter_mock,
    s3_credentials,
    project_root,
):
    minio_target_dir = os.path.join(
        project_root,
        "test_output/test-bucket",
    )

    old_value = os.getenv(TrainingJob.TENSORBOARD_DIR)
    tb_dir = os.path.join(project_root, "test_output/logs/tensorboard/")

    orig_os_environ = copy.deepcopy(os.environ)
    os.environ[TrainingJob.TENSORBOARD_DIR] = tb_dir
    # Ensure that the configuration gets logged without replacing environment variables.
    # This ensures, that the logged model configuration is generic for all devices.
    os.environ["MEDIA_DIR"] = "/tmp/test_work_dir"

    try:

        training_job = TrainingJob(
            image_locations=[
                S3DataLocation(
                    uri="https://127.0.0.1:9000/test-bucket/images",
                    location_id="TEST_OUTPUT",
                    credentials=s3_credentials,
                )
            ],
            annotation_locations=[
                S3DataLocation(
                    uri="https://127.0.0.1:9000/test-bucket/annotations",
                    location_id="TEST_OUTPUT",
                    credentials=s3_credentials,
                )
            ],
            model_checkpoint_location=S3DataLocation(
                uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
                location_id="TEST_OUTPUT",
                credentials=s3_credentials,
            ),
            model_type_name="test_model",
            model_configuration_dict=TestConfig(unique_name="test_model").to_dict(),
            evaluator_configuration_dict={
                "iou_thresholds": [0.5],
                "annotation_handler_config": AnnotationHandlerConfig(
                    pascal_voc_input_data=[
                        AnnotationHandlerPASCALVOCInputDataConfig(
                            input_image_dir=os.path.join(minio_target_dir, "images"),
                            input_xml_dir=os.path.join(
                                minio_target_dir,
                                "annotations/pascal_voc/dummy_task",
                            ),
                            image_format=".jpg",
                            input_sub_dirs=[],
                        )
                    ],
                ).to_dict(),
            },
            mlflow_adapter=mlflow_adapter_mock,
        )

        training_job.run()

        assert training_job

        assert training_job._mlflow_adapter.log_model_configuration_dict.call_args_list[
            0
        ] == call(
            run_id=mlflow_adapter_mock.credentials.run_id,
            configuration_dict=TestConfig(
                unique_name="test_model", test_config="Hello World"
            ).to_dict(),
        )

        assert len([p for p in Path(tb_dir).glob("**/events.out.tfevents.*")]) == 1
    finally:
        os.environ = orig_os_environ
        shutil.rmtree(tb_dir, ignore_errors=True)


@mark.usefixtures(
    "create_model_mock",
    "determine_config_class_mock",
    "create_model_registry_mock",
    "download_objects_mock",
    "upload_objects_mock",
    "mlflow_active_run_mock",
    "mlflow_adapter_mock_return_value_none",
    "mlflow_tracking_client_mock",
    "create_fp_fn_images_mock",
)
def test___log_results_mlflow_client_is_none(project_root, s3_credentials) -> None:
    """Tests _log_results if the mlflow client is none"""
    minio_target_dir = os.path.join(
        project_root,
        "test_output/test-bucket",
    )

    tb_dir = os.path.join(project_root, "test_output/logs/tensorboard/")
    os.environ[TrainingJob.TENSORBOARD_DIR] = tb_dir
    training_job = TrainingJob(
        image_locations=[
            S3DataLocation(
                uri="https://127.0.0.1:9000/test-bucket/images",
                location_id="TEST_OUTPUT",
                credentials=s3_credentials,
            )
        ],
        annotation_locations=[
            S3DataLocation(
                uri="https://127.0.0.1:9000/test-bucket/annotations",
                location_id="TEST_OUTPUT",
                credentials=s3_credentials,
            )
        ],
        model_checkpoint_location=S3DataLocation(
            uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
            location_id="TEST_OUTPUT",
            credentials=s3_credentials,
        ),
        model_type_name="test_model",
        model_configuration_dict=TestConfig(unique_name="test-model").to_dict(),
        evaluator_configuration_dict={
            "iou_thresholds": [0.5],
            "annotation_handler_config": AnnotationHandlerConfig(
                pascal_voc_input_data=[
                    AnnotationHandlerPASCALVOCInputDataConfig(
                        input_image_dir=os.path.join(minio_target_dir, "images"),
                        input_xml_dir=os.path.join(
                            minio_target_dir,
                            "annotations/pascal_voc/dummy_task",
                        ),
                        image_format=".jpg",
                        input_sub_dirs=[],
                    )
                ],
            ).to_dict(),
        },
        mlflow_adapter=MagicMock(),
    )

    # TODO: define as fixtures
    best_checkpoint_info: CheckpointInfo = CheckpointInfo(
        path="checkpoint-path-1", score=1.0
    )
    dummy_dict: dict = dict()
    model_specifier: str = "model-specifier"
    dummy_dict_metrics_info = {best_checkpoint_info.path: dict()}
    geometric_evaluation_metrics: GeometricEvaluationMetrics = (
        GeometricEvaluationMetrics(
            model_specifier=model_specifier,
            metrics_dict=dummy_dict,
            metrics_image_info_dict=dummy_dict_metrics_info,
        )
    )

    evaluated_checkpoint_metrics: Dict[str, Any] = {
        "checkpoint-path-1": geometric_evaluation_metrics,
        "checkpoint-path-2": geometric_evaluation_metrics,
    }

    return_value: None = training_job._TrainingJob__log_geometric_evaluation_results(
        evaluated_checkpoint_metrics, best_checkpoint_info
    )
    assert return_value is None


@mark.usefixtures(
    "create_model_mock",
    "determine_config_class_mock",
    "create_model_registry_mock",
    "download_objects_mock",
    "upload_objects_mock",
    "mlflow_active_run_mock",
    "mlflow_adapter_mock",
    "mlflow_tracking_client_mock",
    "log_od_metrics_to_mlflow_run_mock",
    "create_fp_fn_images_mock",
    "dummy_image",
    "fp_fn_image_dict",
)
def test___log_results(project_root, s3_credentials) -> None:
    """Tests whether _log_results calls the correct logging functions for given mlflow
    client"""
    minio_target_dir = os.path.join(
        project_root,
        "test_output/test-bucket",
    )

    tb_dir = os.path.join(project_root, "test_output/logs/tensorboard/")
    os.environ[TrainingJob.TENSORBOARD_DIR] = tb_dir

    mlflow_adapter_mock = MagicMock()
    mlflow_adapter_mock.credentials.run_id = "test"

    training_job = TrainingJob(
        image_locations=[
            S3DataLocation(
                uri="https://127.0.0.1:9000/test-bucket/images",
                location_id="TEST_OUTPUT",
                credentials=s3_credentials,
            )
        ],
        annotation_locations=[
            S3DataLocation(
                uri="https://127.0.0.1:9000/test-bucket/annotations",
                location_id="TEST_OUTPUT",
                credentials=s3_credentials,
            )
        ],
        model_checkpoint_location=S3DataLocation(
            uri="https://127.0.0.1:9000/test-bucket/test-bucket/checkpoints/model.pth",
            location_id="TEST_OUTPUT",
            credentials=s3_credentials,
        ),
        model_type_name="test_model",
        model_configuration_dict=TestConfig(unique_name="test-model").to_dict(),
        evaluator_configuration_dict={
            "iou_thresholds": [0.5],
            "annotation_handler_config": AnnotationHandlerConfig(
                pascal_voc_input_data=[
                    AnnotationHandlerPASCALVOCInputDataConfig(
                        input_image_dir=os.path.join(minio_target_dir, "images"),
                        input_xml_dir=os.path.join(
                            minio_target_dir,
                            "annotations/pascal_voc/dummy_task",
                        ),
                        image_format=".jpg",
                        input_sub_dirs=[],
                    )
                ],
            ).to_dict(),
        },
        mlflow_adapter=mlflow_adapter_mock,
    )

    # for some reason you cannot patch the methods via decorators, but setting them here works
    training_job._mlflow_adapter.log_artifact = MagicMock(return_value=None)
    training_job._mlflow_adapter.log_metric = MagicMock(return_value=None)
    training_job._mlflow_adapter.log_param = MagicMock(return_value=None)

    # TODO: define as fixtures
    best_checkpoint_info: CheckpointInfo = CheckpointInfo(
        path="checkpoint-path-1", score=1.0
    )
    model_specifier: str = "model-specifier"
    dummy_dict: dict = dict()
    dummy_dict_metrics_info = {best_checkpoint_info.path: dict()}
    geometric_evaluation_metrics: GeometricEvaluationMetrics = (
        GeometricEvaluationMetrics(
            model_specifier=model_specifier,
            metrics_dict=dummy_dict,
            metrics_image_info_dict=dummy_dict_metrics_info,
        )
    )
    evaluated_checkpoint_metrics: Dict[str, Any] = {
        "checkpoint-path-1": geometric_evaluation_metrics,
        "checkpoint-path-2": geometric_evaluation_metrics,
    }
    dummy_image_expected: np.ndarray = np.zeros([100, 100, 3], dtype=np.uint8)
    fp_fn_image_dict_expected: Dict[str, Dict[str, np.ndarray]] = {
        "fp_fn_img_path_1_ID_1_GT_1_FP_1_FN_1.jpeg": {
            "0_class": dummy_image_expected,
            "1_class": dummy_image_expected,
        },
        "fp_fn_img_path_2_ID_2_GT_1_FP_1_FN_1.jpg": {
            "0_class": dummy_image_expected,
            "1_class": dummy_image_expected,
        },
    }

    return_value: None = training_job._TrainingJob__log_geometric_evaluation_results(
        evaluated_checkpoint_metrics, best_checkpoint_info
    )
    _ = training_job._TrainingJob__log_best_metric(best_checkpoint_info)

    training_job._mlflow_adapter.log_artifact.assert_called_with(
        run_id="test", local_path=best_checkpoint_info.path
    )
    training_job._mlflow_adapter.log_param.assert_has_calls(
        [
            call(
                run_id="test",
                key="best_checkpoint",
                value=os.path.basename(best_checkpoint_info.path),
            ),
            call(run_id="test", key="best_checkpoint_step", value=0),
        ],
        any_order=True,
    )
    training_job._mlflow_adapter.log_metric.assert_called_with(
        run_id="test", key="score", value=best_checkpoint_info.score
    )

    log_image_calls_expected = []
    for (
        fp_fn_image_name,
        class_identifier_images,
    ) in fp_fn_image_dict_expected.items():
        fp_fn_image_name = fp_fn_image_name.split("/")[-1]

        for class_identifier_str, image in class_identifier_images.items():
            log_image_calls_expected.append(
                call(
                    run_id="test",
                    image=dummy_image_expected,
                    artifact_file=os.path.join(
                        "fp_fn_images", class_identifier_str, fp_fn_image_name
                    ),
                )
            )
    training_job._mlflow_adapter.log_image.has_calls(log_image_calls_expected)

    assert return_value is None


# TODO: add tests for method __push_checkpoint
if __name__ == "__main__":
    unittest.main()
