# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import json
import os
from typing import Any, Dict
from unittest.mock import MagicMock, patch

import pytest
from pytest_mock import MockerFixture

from mlcvzoo_service_jobs.jobs.constants import (
    JobTypes,
    PredictionJobParameters,
    SharedJobParameters,
    TrainingJobParameters,
)
from mlcvzoo_service_jobs.runner import Runner
from mlcvzoo_service_jobs.tests.unit_tests.conftest import determine_project_root


def create_annotation_handler_config() -> Dict[str, Any]:
    with open(
        os.path.join(
            str(determine_project_root()), "test_data/annotation_handler_config.json"
        )
    ) as json_file:
        return json.load(json_file)


@pytest.fixture
@patch.dict(os.environ, {SharedJobParameters.JOB_TYPE: JobTypes.PREDICTION.value})
def prediction_runner():
    return Runner()


@pytest.fixture(scope="function")
def download_objects_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.adapters.s3.adapter.S3Adapter.download_objects",
        return_value=({}, ["test.jpg"], ["test_bucket/test.jpg"]),
    )


@pytest.fixture
@patch("mlcvzoo_service_jobs.runner.MlflowAdapter")
@patch("mlcvzoo_service_jobs.runner.MlflowCredentials")
@patch.dict(
    os.environ,
    {
        SharedJobParameters.JOB_TYPE: JobTypes.TRAINING.value,
        TrainingJobParameters.MLFLOW_CREDENTIALS: '{"server_uri": "http://example.com", "s3_endpoint_url": "http://example.com", "s3_region_name": "us-east-1", "s3_artifact_access_key": "key", "s3_artifact_access_id": "id"}',
    },
)
def training_runner(*_):
    return Runner()


def test_job_type_training(training_runner):
    assert training_runner.job_type == JobTypes.TRAINING.value


def test_job_type_prediction(prediction_runner):
    assert prediction_runner.job_type == JobTypes.PREDICTION.value


@patch.dict(
    os.environ,
    {
        SharedJobParameters.JOB_TYPE: "INVALID",
        SharedJobParameters.SERVICE_ENDPOINT: "http://example.com",
        SharedJobParameters.IMAGE_LOCATIONS: "[]",
        SharedJobParameters.MODEL_CHECKPOINT_LOCATION: "{}",
        SharedJobParameters.MODEL_TYPE_NAME: "model",
        SharedJobParameters.MODEL_CONFIGURATION_DICT: "{}",
    },
)
@patch("mlcvzoo_service_jobs.runner.requests.put")
def test_invalid_job_type(mock_requests):
    runner = Runner()
    runner.run()

    mock_requests.assert_called_once_with(
        url="http://example.com",
        json={
            "crashed": True,
            "message": "Job type must one of [<JobTypes.TRAINING: 'TRAINING'>, "
            "<JobTypes.PREDICTION: 'PREDICTION'>]",
        },
    )


@patch.dict(os.environ, {SharedJobParameters.SERVICE_ENDPOINT: "http://example.com"})
def test_get_credential_constant_valid():
    value = Runner.get_credential_constant(SharedJobParameters.SERVICE_ENDPOINT)
    assert value == "http://example.com"


@patch.dict(os.environ, {}, clear=True)
def test_get_credential_constant_missing():
    with pytest.raises(ValueError):
        Runner.get_credential_constant(SharedJobParameters.SERVICE_ENDPOINT)


@patch("requests.put")
def test_post_results_success(mock_put):
    mock_put.return_value.status_code = 200
    Runner._Runner__post_results({"result": "success"}, "http://example.com")
    mock_put.assert_called_once()


@patch("requests.put")
def test_post_results_failure(mock_put):
    mock_put.return_value.status_code = 500
    Runner._Runner__post_results({"result": "failure"}, "http://example.com")
    mock_put.assert_called_once()


@patch.dict(
    os.environ,
    {
        TrainingJobParameters.MLFLOW_CREDENTIALS: '{"server_uri": "http://example.com", "s3_endpoint_url": "http://example.com", "s3_region_name": "us-east-1", "s3_artifact_access_key": "key", "s3_artifact_access_id": "id"}'
    },
)
@patch("mlcvzoo_service_jobs.runner.MlflowAdapter")
def test_setup_mlflow(_):
    adapter = Runner._Runner__setup_mlflow()
    assert adapter is not None


@patch("mlcvzoo_service_jobs.jobs.training.training_job.ModelEvaluatorCLIConfig")
@patch("mlcvzoo_service_jobs.jobs.base.base_job.ModelRegistry")
@patch("mlcvzoo_service_jobs.runner.TrainingJob")
@patch("mlcvzoo_service_jobs.runner.requests.put")
@patch.dict(
    os.environ,
    {
        SharedJobParameters.SERVICE_ENDPOINT: "http://example.com",
        SharedJobParameters.IMAGE_LOCATIONS: "[]",
        SharedJobParameters.MODEL_CHECKPOINT_LOCATION: '{"uri": "s3://bucket/model.pth", "credentials": {"s3_endpoint_url": "http://example.com", "s3_region_name": "us-east-1", "s3_artifact_access_key": "key", "s3_artifact_access_id": "id"}}',
        SharedJobParameters.MODEL_TYPE_NAME: "model",
        SharedJobParameters.MODEL_CONFIGURATION_DICT: "{}",
        TrainingJobParameters.EVAL_CONFIGURATION_DICT: json.dumps(
            {
                "iou_thresholds": 0.5,
                "annotation_handler_config": create_annotation_handler_config(),
            }
        ),
        TrainingJobParameters.ANNOTATION_LOCATIONS: "[]",
    },
)
def test_run_training_job(
    mock_requests,
    mock_training_job,
    mock_model_registry,
    mock_model_eval_cli,
    training_runner,
):
    mock_training_job.return_value.run.return_value = {}

    training_runner.run()

    mock_training_job.return_value.run.assert_called_once()
    mock_requests.assert_called_once_with(url="http://example.com", json={})


@patch("mlcvzoo_service_jobs.runner.PredictionJob")
@patch("mlcvzoo_service_jobs.runner.requests.put")
@patch.dict(
    os.environ,
    {
        SharedJobParameters.JOB_TYPE: JobTypes.PREDICTION.value,
        SharedJobParameters.SERVICE_ENDPOINT: "http://example.com",
        SharedJobParameters.IMAGE_LOCATIONS: "[]",
        SharedJobParameters.MODEL_CHECKPOINT_LOCATION: '{"uri": "s3://bucket/model.pth", "credentials": {"s3_endpoint_url": "http://example.com", "s3_region_name": "us-east-1", "s3_artifact_access_key": "key", "s3_artifact_access_id": "id"}}',
        SharedJobParameters.MODEL_TYPE_NAME: "model",
        SharedJobParameters.MODEL_CONFIGURATION_DICT: "{}",
    },
)
def test_run_prediction_job(mock_requests, mock_prediction_job, prediction_runner):
    mock_prediction_job.return_value.run.return_value = {}
    prediction_runner.run()

    mock_prediction_job.return_value.run.assert_called_once()
    mock_requests.assert_called_once_with(url="http://example.com", json={})


@patch("mlcvzoo_service_jobs.runner.PredictionJob")
@patch.dict(
    os.environ,
    {
        SharedJobParameters.JOB_TYPE: JobTypes.PREDICTION.value,
        SharedJobParameters.SERVICE_ENDPOINT: "http://example.com",
        SharedJobParameters.IMAGE_LOCATIONS: "[]",
        SharedJobParameters.MODEL_CHECKPOINT_LOCATION: '{"uri": "s3://bucket/model.pth", "credentials": {"s3_endpoint_url": "http://example.com", "s3_region_name": "us-east-1", "s3_artifact_access_key": "key", "s3_artifact_access_id": "id"}}',
        SharedJobParameters.MODEL_TYPE_NAME: "model",
        SharedJobParameters.MODEL_CONFIGURATION_DICT: "{}",
        PredictionJobParameters.SECOND_MODEL_CHECKPOINT_LOCATION: '{"uri": "s3://bucket/model.pth", "credentials": {"s3_endpoint_url": "http://example.com", "s3_region_name": "us-east-1", "s3_artifact_access_key": "key", "s3_artifact_access_id": "id"}}',
        PredictionJobParameters.SECOND_MODEL_TYPE_NAME: "model",
        PredictionJobParameters.SECOND_MODEL_CONFIGURATION_DICT: "{}",
    },
)
@patch("mlcvzoo_service_jobs.runner.requests.put")
def test_run_prediction_job_with_second_model(
    mock_requests, mock_prediction_job, prediction_runner
):
    mock_prediction_job.return_value.run.return_value = {}
    prediction_runner.run()

    mock_prediction_job.return_value.run.assert_called_once()
    mock_requests.assert_called_once_with(url="http://example.com", json={})


@patch("mlcvzoo_service_jobs.runner.TrainingJob")
@patch.dict(
    os.environ,
    {
        SharedJobParameters.SERVICE_ENDPOINT: "http://example.com",
        SharedJobParameters.IMAGE_LOCATIONS: "[]",
        SharedJobParameters.MODEL_CHECKPOINT_LOCATION: '{"uri": "s3://bucket/model.pth", "credentials": {"s3_endpoint_url": "http://example.com", "s3_region_name": "us-east-1", "s3_artifact_access_key": "key", "s3_artifact_access_id": "id"}}',
        SharedJobParameters.MODEL_TYPE_NAME: "model",
        SharedJobParameters.MODEL_CONFIGURATION_DICT: "{}",
        TrainingJobParameters.EVAL_CONFIGURATION_DICT: "{}",
        TrainingJobParameters.ANNOTATION_LOCATIONS: "[]",
    },
)
@patch("mlcvzoo_service_jobs.runner.requests.put")
def test_run_training_job_crash(mock_requests, mock_training_job, training_runner):
    mock_training_job.side_effect = Exception("Training job crashed")
    training_runner.run()
    mock_training_job.assert_called_once()
