# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

from unittest.mock import patch

from mlcvzoo_service_jobs.main import main


@patch("mlcvzoo_service_jobs.main.Runner")
def test_main(mock_runner):
    main()

    mock_runner.return_value.run.assert_called_once()
