# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Utils for testing."""


import related
from mlcvzoo_base.api.configuration import ModelConfiguration


class TestConfig(ModelConfiguration):
    test_config: str = related.StringField(required=False, default="Hello World")
