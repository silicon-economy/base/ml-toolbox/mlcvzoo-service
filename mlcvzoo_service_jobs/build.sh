#!/bin/sh

TARGET_PYTHON_VERSION=$(python3 -c 'import platform;print("".join(platform.python_version().split(".")[0:2]))')

REQUIREMENTS_FILE="${REQUIREMENTS_FILE:=./requirements_locked/requirements-lock-uv-py$TARGET_PYTHON_VERSION-all.txt}"

if [ -z $CUDA_VERSION_REDUCED ]; then
  CUDA_VERSION_REDUCED=$($CUDA_HOME/bin/nvcc --version | grep 'Cuda compilation tools' | awk '{print $5}' | tr -d . | tr -d ,)
fi

# Currenctly there are network issues with uv pip sync, therefore install via pip directly
#uv pip sync "$REQUIREMENTS_FILE"
python3 -m pip install -r "$REQUIREMENTS_FILE"

# Install YOLOX - due to bugs it is not a direct dependency of mlcvzoo-yolox anymore
if python3 -c "import mlcvzoo_yolox" &> /dev/null; then
  # Ensure setuptools and wheel
  python3 -m pip install \
    --no-cache \
    --no-cache-dir \
    --no-deps \
    --no-build-isolation \
    --no-use-pep517 \
    "git+https://github.com/Megvii-BaseDetection/YOLOX.git@419778480ab6ec0590e5d3831b3afb3b46ab2aa3"
fi

# Reinstall and compile mmcv with legacy setup, since the pulled pypi package does not fit to the system environment
if python -c "import mmcv" &> /dev/null; then
  export TORCH_VERSION="$(python3 -m pip show torch | grep Version | awk '{print $2}')"
  export MMCV_VERSION="$(python3 -m pip show mmcv | grep Version | awk '{print $2}')"

  python3 -m pip uninstall -y mmcv

  # Try to install a prebuilt version from openmmlab.
  # A complete list of version compatibility between mmcv, torch and cuda can be found here:
  # https://mmcv.readthedocs.io/en/latest/get_started/installation.html#install-with-pip
  python3 -m pip install \
    --no-cache mmcv=="$MMCV_VERSION" \
    --no-index \
    --find-links "https://download.openmmlab.com/mmcv/dist/cu$CUDA_VERSION_REDUCED/torch$TORCH_VERSION/index.html"

  if [ "$?" -eq 0 ]; then
    echo "Install of mmcv from mirror succeeded"
  else
    python3 -m pip uninstall -y mmcv
    echo "Install of mmcv from mirror failed, compile mmcv package..."
    python3 -m pip install setuptools wheel
    # MMCV_WITH_OPS=1 is needed to build the full version with CPU
    # MMCV_WITH_OPS=0 would build the lite version of mmcv
    MMCV_WITH_OPS=1 python3 -m pip install \
      --no-cache \
      --no-binary :all: \
      --no-deps \
      --no-build-isolation \
      --no-use-pep517 \
      mmcv=="$MMCV_VERSION"
  fi
fi
