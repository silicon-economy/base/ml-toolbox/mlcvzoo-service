# MLCVZoo Service

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_service** is a REST service to train
arbitrary MLCVZoo models.

## Setup

NOTE: In order to use the GPU in docker container you need to setup the nvidia-container-runtime

### Build

The MLCVZoo Service uses a specific Docker runtime image for the execution of training and
inference.
To build the i
mages, execute the following commands from the root of the project:

MLCVZoo Service image:
```
docker build -t silicon-economy/base/ml-toolbox/mlcvzoo-service/mlcvzoo-service:latest -f mlcvzoo_service/Dockerfile.runtime mlcvzoo_service/
```

MLCVZoo Servic Jobs Image:
```
docker build -t silicon-economy/base/ml-toolbox/mlcvzoo-service/mlcvzoo-service-jobs:latest  -f mlcvzoo_service_jobs/Dockerfile.runtime mlcvzoo_service_jobs/
```

### Config

The MLCVZoo Service requires specific environment variables to be set for configuration. Before
start, define the following variables according to your setup:

| Variable                      | Description                                                                                                                                                        |
|-------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GPUS                          | Comma separated list of available GPUs for training and inference e.g. "0,1,2" if GPUs with IDs 0, 1 and 2 are available                                           |
| NETWORK_MODE                  | If the MLCVZoo Service is used locally or for development purposes use NETWORK_MODE=host. Default to bridge                                                        |
| SERVICE_ENDPOINT              | Endpoint of the service.                                                                                                                                           |
| CONTAINER_SERVICE             | Container technology service to use. Defaults to DOCKER                                                                                                            |
| AUTO_REMOVE_CONTAINER         | Whether to automatically remove containers after they are finished. Per default, the auto removal is enabled                                                       |
| YOLOX_RUNTIME_IMAGE           | Docker image name for training mlcvzoo-yolox models                                                                                                                |
| MMDETECTION_RUNTIME_IMAGE     | Docker image name for training mlcvzoo-mmdetection models                                                                                                          |
| MMOCR_RUNTIME_IMAGE           | Docker image name for training mlcvzoo-mmocr models                                                                                                                |
| MMPRETRAIN_RUNTIME_IMAGE      | Docker image name for training mlcvzoo-mmpretrain models                                                                                                           |
| MMROTATE_RUNTIME_IMAGE        | Docker image name for training mlcvzoo-mmrotate models                                                                                                             |
| MMOCR_DIR                     | Absolute path of the mmocr module in the virtual environment. Necessary to parse configs during mmocr runtime---THIS IS ONLY TEMPORARY FOR DEVELOPMENT PURPOSES--- |
| JOB_LOG_DIR                   | Directory to store job logs. Defaults to WORKING_DIRECTORY/job_logs/<job_id>.txt                                                                                   |


A basic configuration file can be found [here](mlcvzoo_service/config/mlcvzoo-service-config.env).

## Run

NOTE:
- Make sure to use the correct runtime when you want to use GPUS (e.g.
nvidia-container-runtime, or nvidia-docker)
- Make sure that the _GPUS_ environment variable fits to your docker parameter _--gpus_

```
docker run -it --gpus '"device=0"' -v /var/run/docker.sock:/var/run/docker.sock --env-file mlcvzoo_service/config/mlcvzoo-service-config.env silicon-economy/base/ml-toolbox/mlcvzoo-service/mlcvzoo-service:latest
```

# Contact information

Maintainers:

- Maximilian Otten <a href="mailto:maximilian.otten@iml.fraunhofer.de?">
  maximilian.otten@iml.fraunhofer.de</a>

## Technology stack

- Python
