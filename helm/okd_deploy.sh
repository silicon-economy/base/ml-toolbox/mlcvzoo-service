#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=ml-toolbox}"
FULLNAME="mlcvzoo-service"

# Upgrade or install
helm --set-string="fullnameOverride=${FULLNAME}" upgrade -n "$NAMESPACE" -i mlcvzoo-service "$@" . || exit 1
# Ensure image stream picks up the new container image right away
oc -n "$NAMESPACE" import-image "${FULLNAME}"
